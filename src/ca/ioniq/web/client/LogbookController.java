/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ioniq.web.client.activity.BaseActivity;
import ca.ioniq.web.client.event.login.LoginEvent;
import ca.ioniq.web.client.event.login.LoginEventHandler;
import ca.ioniq.web.client.place.NotePlace;
import ca.ioniq.web.client.ui.ErrorMessagePanel;
import ca.ioniq.web.client.ui.util.NoteStatus;
import ca.ioniq.web.shared.AuditPO;
import ca.ioniq.web.shared.NotePO;
import ca.ioniq.web.shared.NoteStatusPO;
import ca.ioniq.web.shared.PilotStatusPO;
import ca.ioniq.web.shared.Role;
import ca.ioniq.web.shared.RolePO;
import ca.ioniq.web.shared.SectorPO;
import ca.ioniq.web.shared.UserPO;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.URL;

public class LogbookController {
	
	private EventBus _eventBus;
	private ClientFactory _clientFactory;
	private ErrorMessagePanel _errorMesagePanel = new ErrorMessagePanel();
	
	// caches  
	private UserPO _currentUser;
	
	private Map<String, List<NotePO>> _noteListBySector = new HashMap<String, List<NotePO>>();
	
	private List<PilotStatusPO> _pilotStatusList = new ArrayList<PilotStatusPO>();
	private Map<Long, PilotStatusPO> _pilotStatusMap = new HashMap<Long, PilotStatusPO>();
	
	private List<RolePO> _roleList = new ArrayList<RolePO>();
	
	private List<SectorPO> _sectorList = new ArrayList<SectorPO>();
	private Map<String, SectorPO> _sectorByNameMap = new HashMap<String, SectorPO>();
	private Map<Long, SectorPO> _sectorByIdMap = new HashMap<Long, SectorPO>();
	
	private List<UserPO> _userList = new ArrayList<UserPO>();
	
	// note status
	private List<NoteStatusPO> _noteStatus = new ArrayList<NoteStatusPO>();
	
	// maps note status presentation object to note status id
	private Map<Long, NoteStatusPO> _noteStatusMap = new HashMap<Long, NoteStatusPO>();
	
	// maps note status to note status presentation object
	private Map<NoteStatus, NoteStatusPO> _noteStatusEmMap = new HashMap<NoteStatus, NoteStatusPO>(); 
	
	// audit
	private Map<String, List<AuditPO>> _auditMapBySector = new HashMap<String, List<AuditPO>>();
	
	public LogbookController(final EventBus eventBus, final ClientFactory clientFactory) {
		_eventBus = eventBus;
		_clientFactory = clientFactory;
		
		bind();
	}
	
	public void bind() {
		_eventBus.addHandler(LoginEvent.TYPE, new LoginEventHandler() {
			@Override
			public void onLogin(LoginEvent event) {
				_clientFactory.getPlaceController().goTo(new NotePlace("view"));
			}
		});
	}

	/**
	 * 		1- get the sector for this note from the sector map by id
	 * 
	 * 		2- get the note list for the sector id
	 * 			2.1- if not list create a new list
	 * 				 else 
	 * 					add the note to the list
	 * 
	 * 		3- add the list to the map with the sector name as key
	 * 
	 * @param noteList
	 */
	public void setNoteList(List<NotePO> noteList) {
		for(NotePO note : noteList) {
			SectorPO noteSector = _sectorByIdMap.get(note.getSectorId());
			List<NotePO> noteBySectorList = _noteListBySector.get(noteSector.getSectorName());
			if(noteBySectorList == null) {
				noteBySectorList = new ArrayList<NotePO>();
			}
			
			if(!noteBySectorList.contains(note)) {
				noteBySectorList.add(note);
			}
			
			_noteListBySector.put(noteSector.getSectorName(), noteBySectorList);
		}
	}

	private void setPilotStatusMap(List<PilotStatusPO> piloStatusList) {
		for(PilotStatusPO pilotStatusPo : piloStatusList) {
			_pilotStatusMap.put(pilotStatusPo.getPilotStatusId(), pilotStatusPo);
		}
	}
	
	public List<PilotStatusPO> getPilotStatusList() {
		if(_pilotStatusList == null || _pilotStatusList.isEmpty()) {
			String url = URL.encode(BaseActivity.PILOTSTATUS_BASE_URL);
			
			RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
			
			try {
				requestBuilder.sendRequest(null, new RequestCallback() {
					@Override
					public void onResponseReceived(Request request, Response response) {
						int responseCode = response.getStatusCode();
						if(responseCode == Response.SC_OK) {
							_pilotStatusList = _clientFactory.getParser().deserializePilotStatusList(response.getText());
							setPilotStatusMap(_pilotStatusList);
						}
					}

					@Override
					public void onError(Request request, Throwable exception) {
						_errorMesagePanel.showErrorMessage(exception.getMessage());
					}
				});
			} catch (RequestException e) {
				_errorMesagePanel.showErrorMessage(ErrorMessagePanel.SERVICE_UNAVAILABLE);
			}
		}
		
		return _pilotStatusList;
	}
	
	public void setRoleList(List<RolePO> roleList) {
		this._roleList = roleList;
	}

	public List<RolePO> getRoleList() {
		if(_roleList == null || _roleList.isEmpty()) {
			_roleList =  Arrays.asList(
					new RolePO(new Long(1), Role.NO_ROLE.toString(), Role.NO_ROLE.toString()),
					new RolePO(new Long(1), Role.DISPATCHER.toString(), Role.DISPATCHER.toString()),
					new RolePO(new Long(2), Role.SUPERVISOR_DISPATCH.toString(), Role.SUPERVISOR_DISPATCH.toString()),
					new RolePO(new Long(3), Role.DIRECTOR.toString(), Role.DIRECTOR.toString()),
					new RolePO(new Long(4), Role.ADMIN.toString(), Role.ADMIN.toString())
				);
		}
		
		return _roleList;
	}
	
	private void setSectorMap(List<SectorPO> sectorList) {
		for(SectorPO sector : sectorList) {
			_sectorByNameMap.put(sector.getSectorName(), sector);
			_sectorByIdMap.put(sector.getSectorId(), sector);
		}
	}

	public List<SectorPO> getSectorList() {
		if(_sectorList == null || _sectorList.isEmpty()) {		
			String url = URL.encode(BaseActivity.SECTOR_BASE_URL);
			
			RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
			
			try {
				requestBuilder.sendRequest(null, new RequestCallback() {
					@Override
					public void onResponseReceived(Request request, Response response) {
						int responseCode = response.getStatusCode();
						if(responseCode == Response.SC_OK) {
							_sectorList = _clientFactory.getParser().deserializeSector(response.getText());
							setSectorMap(_sectorList);
						}
					}

					@Override
					public void onError(Request request, Throwable exception) {
						_errorMesagePanel.showErrorMessage("Error " + exception.getMessage());
					}
				});

			} catch (RequestException e) {
				_errorMesagePanel.showErrorMessage(ErrorMessagePanel.SERVICE_UNAVAILABLE);
			}
		}
			
		return _sectorList;
	}
	
	private void setNoteStatusMap(List<NoteStatusPO> noteStatusList) {
		for(NoteStatusPO noteStatusPo : noteStatusList) {
			_noteStatusMap.put(noteStatusPo.getNoteStatusId(), noteStatusPo);
			_noteStatusEmMap.put(NoteStatus.find(noteStatusPo.getNoteStatusName()), noteStatusPo);
		}
	}
	
	public List<NoteStatusPO> getNoteStatusList() {
		if(_noteStatus == null || _noteStatus.isEmpty()) {
			String url = URL.encode(BaseActivity.NOTESTATUS_BASE_URL);
			RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
			
			try {
				requestBuilder.sendRequest(null, new RequestCallback() {
					@Override
					public void onResponseReceived(Request request, Response response) {
						int responseCode = response.getStatusCode();
						if(responseCode == Response.SC_OK) {
							_noteStatus = _clientFactory.getParser().deserializeNoteStatusList(response.getText());
							setNoteStatusMap(_noteStatus);
						}
					}
					
					@Override
					public void onError(Request request, Throwable exception) {
						_errorMesagePanel.showErrorMessage("Error " + exception.getMessage());
					}
				});
			} catch (RequestException e) {
				_errorMesagePanel.showErrorMessage(ErrorMessagePanel.SERVICE_UNAVAILABLE);
			}
		}
		
		return _noteStatus;
	}
	
	public List<UserPO> getUsersList() {
		if(_userList == null || _userList.isEmpty()) {
			String url = URL.encode(BaseActivity.USER_BASE_URL + "/users");
			RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
			
			try {
				requestBuilder.sendRequest(null, new RequestCallback() {
					@Override
					public void onResponseReceived(Request request, Response response) {
						int responseCode = response.getStatusCode();
						if(responseCode == Response.SC_OK) {
							_userList = _clientFactory.getParser().deserializeUserList(response.getText());
						}
					}

					@Override
					public void onError(Request request, Throwable exception) {
						_errorMesagePanel.showErrorMessage("Error " + exception.getMessage());
					}
				});

			} catch (RequestException e) {
				_errorMesagePanel.showErrorMessage(ErrorMessagePanel.SERVICE_UNAVAILABLE);
			}
		}
		
		return _userList;
	}
	
	public void setCurrentUser(UserPO user) {
		_currentUser = user;
	}
	
	public UserPO getCurrentUser() {
		return _currentUser;
	}

	public Map<Long, NoteStatusPO> getNoteStatusMap() {
		return _noteStatusMap;
	}

	public Map<NoteStatus, NoteStatusPO> getNoteStatusEmMap() {
		return _noteStatusEmMap;
	}

	public Map<Long, PilotStatusPO> getPilotStatusMap() {
		return _pilotStatusMap;
	}

	public void setUserList(List<UserPO> userList) {
		_userList = userList;
	}

	public void setAuditList(List<AuditPO> auditList) {
		for(AuditPO auditPo : auditList) {
			List<AuditPO> auditListForMap = _auditMapBySector.get(auditPo.getSector());
			if(auditListForMap == null) {
				auditListForMap = new ArrayList<AuditPO>();
			}
			
			auditListForMap.add(auditPo);
			_auditMapBySector.put(auditPo.getSector(), auditListForMap);
		}
	}
	
	public List<AuditPO> getAuditForSector(String sectorName) {
		return _auditMapBySector.get(sectorName); 
	}
	
	public SectorPO getSectorById(Long sectorId) {
		return _sectorByIdMap.get(sectorId);
	}
	
	public SectorPO getSectorByName(String sectorName) {
		return _sectorByNameMap.get(sectorName);
	}
}
