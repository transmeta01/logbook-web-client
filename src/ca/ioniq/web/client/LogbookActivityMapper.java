/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client;

import ca.ioniq.web.client.activity.AdminActivity;
import ca.ioniq.web.client.activity.AuditActivity;
import ca.ioniq.web.client.activity.LoginActivity;
import ca.ioniq.web.client.activity.NoteActivity;
import ca.ioniq.web.client.place.AdminPlace;
import ca.ioniq.web.client.place.AuditPlace;
import ca.ioniq.web.client.place.LoginPlace;
import ca.ioniq.web.client.place.NotePlace;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class LogbookActivityMapper implements ActivityMapper {
	
	private ClientFactory clientFactory;
	
	private Place auditPlace;
	private Place notePlace;
	private Place loginPlace;
	private Place adminPlace;
	
	private String START_TOKEN = "start";
	
	public LogbookActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		
		auditPlace 		= new AuditPlace();
		notePlace 		= new NotePlace(START_TOKEN);
		loginPlace 		= new LoginPlace();
		adminPlace 		= new AdminPlace(START_TOKEN); 
	}

	@Override
	public Activity getActivity(Place place) {
		if(clientFactory.getApplicationController().getCurrentUser() != null) {
			if(place instanceof LoginPlace) {
				return new LoginActivity(clientFactory);
			}
			
			if(place instanceof AuditPlace) { 
				return new AuditActivity(new AuditPlace(), clientFactory);
			}
			
			if(place instanceof AdminPlace) {  
				return new AdminActivity(clientFactory); 
			}
			
			if(place instanceof NotePlace) {
				return new NoteActivity(new NotePlace(START_TOKEN), clientFactory);
			}
		} else {
			return new LoginActivity(clientFactory);
		}
		
		return null;
	}

	public Place getAuditPlace() {
		return auditPlace;
	}

	public Place getNotePlace() {
		return notePlace;
	}

	public Place getLoginPlace() {
		return loginPlace;
	}

	public Place getAdminPlace() {
		return adminPlace;
	}
}
