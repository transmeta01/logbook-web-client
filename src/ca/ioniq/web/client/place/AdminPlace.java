package ca.ioniq.web.client.place;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class AdminPlace extends BasePlace {
	
	public AdminPlace(String token) {
		super(token);
	}
	
	public AdminPlace() { super(); }
	

	@Prefix("admin")
	public static class Tokenizer implements PlaceTokenizer<AdminPlace> {

		@Override
		public AdminPlace getPlace(String token) {
			return new AdminPlace(token);
		}

		@Override
		public String getToken(AdminPlace place) {
			return place.getToken();
		}
	}
}
