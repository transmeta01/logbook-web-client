package ca.ioniq.web.client.place;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class AuditPlace extends BasePlace {
	
	public AuditPlace(String token) {
		super(token);
	}
	
	public AuditPlace() { super(); }
	
	@Prefix("audit")
	public static class Tokenizer implements PlaceTokenizer<AuditPlace> {

		@Override
		public AuditPlace getPlace(String token) {
			return new AuditPlace(token);
		}

		@Override
		public String getToken(AuditPlace place) {
			return place.getToken();
		}
	}
}
