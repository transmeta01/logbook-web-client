package ca.ioniq.web.client.place;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class LoginPlace extends BasePlace {
	
	public LoginPlace(String token) {
		super(token);
	}
	
	public LoginPlace() { super(); }
	
	@Prefix("login")
	public static class Tokenizer implements PlaceTokenizer<LoginPlace> {

		@Override
		public LoginPlace getPlace(String token) {
			return new LoginPlace(token);
		}

		@Override
		public String getToken(LoginPlace place) {
			return place.getToken();
		}
	}
}
