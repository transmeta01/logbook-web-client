package ca.ioniq.web.client.place;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class NotePlace extends BasePlace {
	
	public NotePlace(String token) {
		super(token);
	}
	
	@Prefix("note")
	public static class Tokenizer implements PlaceTokenizer<NotePlace> {

		@Override
		public NotePlace getPlace(String token) {
			return new NotePlace(token);
		}

		@Override
		public String getToken(NotePlace place) {
			return place.getToken();
		}
	}
}
