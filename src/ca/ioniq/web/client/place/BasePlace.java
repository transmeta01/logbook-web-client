package ca.ioniq.web.client.place;

import com.google.gwt.place.shared.Place;

public class BasePlace extends Place {
	private String token;
	
	public BasePlace() {}
	
	public BasePlace(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}
}
