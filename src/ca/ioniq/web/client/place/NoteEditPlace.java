package ca.ioniq.web.client.place;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class NoteEditPlace extends BasePlace {
	
	public NoteEditPlace(String token) {
		super(token);
	}
	
	public NoteEditPlace() { super(); }
	
	@Prefix("noteEdit")
	public static class Tokenizer implements PlaceTokenizer<NoteEditPlace> {

		@Override
		public NoteEditPlace getPlace(String token) {
			return new NoteEditPlace(token);
		}

		@Override
		public String getToken(NoteEditPlace place) {
			return place.getToken();
		}
	}
}
