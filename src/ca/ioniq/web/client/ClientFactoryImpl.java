/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Date;
import java.util.List;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.i18n.client.DateTimeFormat;

import ca.ioniq.web.client.ui.audit.AuditViewImpl;
import ca.ioniq.web.client.ui.note.NoteViewImpl;
import ca.ioniq.web.client.resources.TableResource;
import ca.ioniq.web.client.ui.admin.AdminView;
import ca.ioniq.web.client.ui.admin.AdminViewImpl;
import ca.ioniq.web.client.ui.audit.AuditView;
import ca.ioniq.web.client.ui.login.LoginView;
import ca.ioniq.web.client.ui.login.LoginViewImpl;
import ca.ioniq.web.client.ui.login.LogoutPanel;
import ca.ioniq.web.client.ui.menu.AdminMenu;
import ca.ioniq.web.client.ui.menu.ApplicationMenu;
import ca.ioniq.web.client.ui.menu.SectorMenuPanel;
import ca.ioniq.web.client.ui.note.NoteEditDialog;
import ca.ioniq.web.client.ui.note.NoteView;
import ca.ioniq.web.client.ui.pilotStatus.PilotStatusAdminView;
import ca.ioniq.web.client.ui.user.UserAdminView;
import ca.ioniq.web.shared.parser.CustomJSONParser;

public final class ClientFactoryImpl implements ClientFactory {
	
	private final EventBus eventBus;
	private final PlaceController placeController;
	
	private final LogbookController applicationController;
	
	private final ActivityMapper activityMapper;
	private final ActivityManager activityManager;
	
	private final PlaceHistoryMapper historyMapper;
	private final PlaceHistoryHandler placeHistoryHandler;
	
	// main content panel
	private FlowPanel mainContentPanel = new FlowPanel();
	private FlowPanel headerPanel = new FlowPanel();
	
	private final LoginView loginView;
	
	private final FlowPanel mainMenu;
	
	private SectorMenuPanel sectorMenu;
	private ApplicationMenu applicationMenu;
	private AdminMenu adminMenu;
	
	private LogoutPanel logoutPanel;
	
	private AdminView adminView;
	private UserAdminView userAdminView;
	private PilotStatusAdminView pilotStatusAdminView;
	
	private NoteView noteView;
	private NoteEditDialog noteEditDialog;
	
	private AuditView auditView;
	
	private CustomJSONParser parser;
	
	public ClientFactoryImpl() {
		CellTable.Resources tableResource = GWT.create(TableResource.class);
		
		eventBus = new SimpleEventBus();
		placeController = new PlaceController(eventBus, new LogbookDelegate());
		
		activityMapper = new LogbookActivityMapper(this);
		activityManager = new ActivityManager(activityMapper, eventBus);
		
		historyMapper = GWT.create(LogbookPlaceHistoryMapper.class);
		placeHistoryHandler = new PlaceHistoryHandler(historyMapper);
		
		applicationMenu = new ApplicationMenu(this);
		
		loginView = new LoginViewImpl();
		logoutPanel = new LogoutPanel(this);
		
		applicationController = new LogbookController(eventBus, this);
		
		mainContentPanel.getElement().setId("main-container");
		
		headerPanel.getElement().setId("header");
		headerPanel.add(logoutPanel);
		
		mainContentPanel.add(headerPanel);
		
		/* main menu panel */
		mainMenu = new FlowPanel();
		mainMenu.getElement().setId("main-menu");
		
		sectorMenu = new SectorMenuPanel(this);
		mainMenu.add(sectorMenu);
		
		mainMenu.add(applicationMenu);
		
		adminMenu = new AdminMenu(this);
		
		mainContentPanel.add(mainMenu);
		
		// note dialogs
		noteEditDialog = new NoteEditDialog(this);
		// note view
		noteView = new NoteViewImpl(this);
		
		// admin view
		userAdminView = new UserAdminView(this, tableResource);
		pilotStatusAdminView = new PilotStatusAdminView(this, tableResource);
		adminView = new AdminViewImpl(this);
		
		// audit view 
		auditView = new AuditViewImpl(this, tableResource);
		
		parser = new CustomJSONParser();
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public PlaceController getPlaceController() {
		return placeController;
	}

	@Override
	public LogbookController getApplicationController() {
		return applicationController;
	}

	@Override
	public ActivityManager getActivityManager() {
		return activityManager;
	}

	@Override
	public ActivityMapper getActivityMapper() {
		return activityMapper;
	}

	@Override
	public PlaceHistoryMapper getPlaceHistoryMapper() {
		return historyMapper;
	}

	@Override
	public PlaceHistoryHandler getPlaceHistoryHandler() {
		return placeHistoryHandler;
	}

	@Override
	public FlowPanel getMainContentPanel() {
		return mainContentPanel;
	}

	public PlaceHistoryMapper getHistoryMapper() {
		return historyMapper;
	}
	
	@Override
	public LoginView getLoginView() {
		return loginView;
	}

	@Override
	public SectorMenuPanel getSectorMenu() {
		return sectorMenu;
	}

	@Override
	public ApplicationMenu getApplicationMenu() {
		return applicationMenu;
	}

	@Override
	public AdminMenu getAdminMenu() {
		return adminMenu;
	}
	
	@Override
	public FlowPanel getMainMenu() {
		return mainMenu;
	}

	@Override
	public LogoutPanel getLogoutPanel() {
		return logoutPanel;
	}
	
	@Override
	public  AdminView getAdminView() {
		return adminView;
	}

	@Override
	public FlowPanel getHeaderPanel() {
		return headerPanel;
	}

	@Override
	public NoteView getNoteView() {
		return noteView;
	}

	@Override
	public NoteEditDialog getNoteEditDialog() {
		return noteEditDialog;
	}

	@Override
	public UserAdminView getUserAdminView() {
		return userAdminView;
	}

	@Override
	public PilotStatusAdminView getPilotStatusAdminView() {
		return pilotStatusAdminView;
	}
	
	@Override
	public AuditView getAuditView() {
		return auditView;
	}

	@Override
	public CustomJSONParser getParser() {
		return parser;
	}
	
	@Override
	public boolean isSameDay(Date date1, Date date2) {
		String date1String = DateTimeFormat.getFormat("dd MM yyyy").format(date1);
		String date2String = DateTimeFormat.getFormat("dd MM yyyy").format(date2);
		
		return date1String.equals(date2String);
	}
	
	@Override
	public boolean isBefore(Date date1, Date date2) {
		String date1String = DateTimeFormat.getFormat("dd MM yyyy").format(date1);
		String date2String = DateTimeFormat.getFormat("dd MM yyyy").format(date2);
		
		return isBefore(date1String, date2String);
	}
	
	@Override
	public boolean isBefore(String dateString1, String dateString2) {
		boolean isBefore = false;
		List<String> tokenList1 = Arrays.asList(dateString1.split(" "));
		List<String> tokenList2 = Arrays.asList(dateString2.split(" "));
		
		List<Integer> dayValues1 = new ArrayList<Integer>();
		List<Integer> dayValues2 = new ArrayList<Integer>();

		Iterator<String> it1 = tokenList1.iterator();
		Iterator<String> it2 = tokenList2.iterator();
		while(it1.hasNext() && it2.hasNext()) {
			dayValues1.add(Integer.parseInt(it1.next()));
			dayValues2.add(Integer.parseInt(it2.next()));
		}
		
		isBefore = dayValues1.get(2) < dayValues2.get(2) || dayValues1.get(1) < dayValues2.get(1) ||  dayValues1.get(0) < dayValues2.get(0);
		
		return isBefore;
	}
	
}
