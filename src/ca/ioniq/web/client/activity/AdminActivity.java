/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.event.login.LoginEvent;
import ca.ioniq.web.client.ui.ErrorMessagePanel;
import ca.ioniq.web.client.ui.admin.AdminView;
import ca.ioniq.web.shared.PilotStatusPO;
import ca.ioniq.web.shared.UserPO;

public class AdminActivity extends BaseActivity implements AdminView.Presenter {
	
	public AdminActivity(ClientFactory clientFactory) {
		super(clientFactory);
	}

	private AdminView adminView;
	
	private ErrorMessagePanel messageBox = new ErrorMessagePanel();
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		_clientFactory.getAdminMenu().setVisible(Boolean.TRUE);
		_clientFactory.getApplicationMenu().setVisible(Boolean.TRUE);
		
		adminView = _clientFactory.getAdminView();
		
		panel.setWidget(adminView.asWidget());
		
		// if user is null redirect to login view
		if(_clientFactory.getApplicationController().getCurrentUser() == null) {
			_clientFactory.getEventBus().fireEvent(new LoginEvent());
		}
		
		callServer();
	}

	private void callServer() {
		List<UserPO> userList = _clientFactory.getApplicationController().getUsersList();
		if(userList == null || userList.isEmpty()) {
			
			String url = URL.encode(USER_BASE_URL + "/users");
			
			RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
			try {
				RequestCallback operationCallBack = new RequestCallback() {
					@Override
					public void onResponseReceived(Request request, Response response) {
						int responseCode = response.getStatusCode();

						if (responseCode == Response.SC_OK) {
							String contentType = response.getHeader("Content-Type");
							
							List<UserPO> usersList = parseUserResponse(response.getText(), contentType);
							_clientFactory.getApplicationController().setUserList(usersList);
							
							// set list of notes to be displayed												
							adminView.setUserList(usersList);
							
						} else if (responseCode >= Response.SC_INTERNAL_SERVER_ERROR) {
							messageBox.showErrorMessage(ErrorMessagePanel.SERVICE_UNAVAILABLE);
						} else {
							messageBox.showErrorMessage(response.getStatusText());
						}
					}
				
					@Override
					public void onError(Request request, Throwable exception) {
						messageBox.showErrorMessage(exception.getMessage());
					}
				};

				requestBuilder.sendRequest(null, operationCallBack);

			} catch (RequestException e) {
				messageBox.showErrorMessage(e.getMessage());
			}
		}
				
		List<PilotStatusPO> pilotStatusList = _clientFactory.getApplicationController().getPilotStatusList();
		if(pilotStatusList == null || pilotStatusList.isEmpty()) {
			String url = URL.encode(PILOTSTATUS_BASE_URL);

			RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
			try {
				RequestCallback operationCallBack = new RequestCallback() {
					@Override
					public void onResponseReceived(Request request, Response response) {
						int responseCode = response.getStatusCode();

						if (responseCode == Response.SC_OK) {
							String contentType = response.getHeader("Content-Type");
							
							List<PilotStatusPO> statusList = parseResponseForPilotStatus(response.getText(), contentType);

							// set list of notes to be displayed												
							adminView.setPilotStatusList(statusList);
							
						} else if (responseCode >= Response.SC_INTERNAL_SERVER_ERROR) {
							messageBox.showErrorMessage(ErrorMessagePanel.SERVICE_UNAVAILABLE);
						} else {
							messageBox.showErrorMessage(response.getStatusText());
						}
					}
				
					@Override
					public void onError(Request request, Throwable exception) {
						messageBox.showErrorMessage(exception.getMessage());
					}
				};

				requestBuilder.sendRequest(null, operationCallBack);

			} catch (RequestException e) {
				messageBox.showErrorMessage(e.getMessage());
			}
			
		}
		
		adminView.setUserList(userList);
		adminView.setPilotStatusList(pilotStatusList);
		
		adminView.setRoleList(_clientFactory.getApplicationController().getRoleList());
	}
	
	@Override
	public void goTo(Place newPlace) {
		super.goTo(newPlace);
	}
	
	private List<PilotStatusPO> parseResponseForPilotStatus(String serverResponse, String dataType) {
		List<PilotStatusPO> statusList = new ArrayList<PilotStatusPO>();
		if (dataType.equals(JSON_TYPE)) {
			return parseJSONDataForPilotStatus(serverResponse);
		}

		if (dataType.equals(XML_TYPE)) {
			return parseXMLData(serverResponse);
		}

		return statusList;
	}
					

	private List<PilotStatusPO> parseXMLData(String serverResponse) {
		return new ArrayList<PilotStatusPO>();
	}

	private List<PilotStatusPO> parseJSONDataForPilotStatus(String data) {
		return _clientFactory.getParser().deserializePilotStatusList(data);
	}

	private List<UserPO> parseUserResponse(String serverResponse, String dataType) {
		List<UserPO> userList = new ArrayList<UserPO>();
		if (dataType.equals(JSON_TYPE)) {
			return parseUserJSONData(serverResponse);
		}

		if (dataType.equals(XML_TYPE)) {
			return parseUserXMLData(serverResponse);
		}

		return userList;
	}
	
	private List<UserPO> parseUserXMLData(String serverResponse) {
		return new ArrayList<UserPO>();
	}

	private List<UserPO> parseUserJSONData(String data) {
		return _clientFactory.getParser().deserializeUserList(data);
	}
}
