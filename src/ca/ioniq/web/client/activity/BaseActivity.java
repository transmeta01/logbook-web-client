/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.activity;

import ca.ioniq.web.client.ClientFactory;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.Place;

abstract public class BaseActivity extends AbstractActivity {
	
	public final static String JSON_TYPE = "application/json";
	public final static String XML_TYPE = "application/xml";
	public final static String BASE_URL = GWT.getHostPageBaseURL();
	public final static String CONTENT_TYPE = "Content-Type";
	public final static String LOCATION_HEADER = "Location";
	
	public final static String NOTE_BASE_URL = "rest/note";
	public final static String ATTACHMENT_BASE_URL = "rest/note/attachment";
	public final static String ROLE_BASE_URL = "rest/role";
	public final static String USER_BASE_URL = "rest/user";
	public final static String AUDIT_BASE_URL = "rest/audit";
	public final static String SECTOR_BASE_URL = "rest/sector";
	public final static String NOTESTATUS_BASE_URL = "rest/note/notestatus";
	public final static String PILOTSTATUS_BASE_URL = "rest/pilotstatus";
	
	// package protected
	ClientFactory _clientFactory;
	
	public BaseActivity(final ClientFactory clientFactory) {
		_clientFactory = clientFactory;
	}
	
	public void goTo(Place newPlace) {
		_clientFactory.getPlaceController().goTo(newPlace);
	}
}
