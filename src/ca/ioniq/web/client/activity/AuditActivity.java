/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.activity;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.event.shared.EventBus;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.event.audit.ViewAuditBySectorEvent;
import ca.ioniq.web.client.ui.audit.AuditView;

public class AuditActivity extends BaseActivity implements AuditView.Presenter {
	private AuditView auditView;
	
	public AuditActivity(Place place, final ClientFactory clientFactory) {
		super(clientFactory);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		auditView = _clientFactory.getAuditView();
		auditView.setPresenter(this);
		panel.setWidget(auditView.asWidget());
		
		auditView.setSectorList(_clientFactory.getApplicationController().getSectorList());
		auditView.setRoleList(_clientFactory.getApplicationController().getRoleList());
		
		String currentActiveSector = _clientFactory.getSectorMenu().getActive().getText();
		_clientFactory.getEventBus().fireEvent(new ViewAuditBySectorEvent(currentActiveSector));
	}

	@Override
	public void goTo(Place place) {
		super.goTo(place);
	}
}
