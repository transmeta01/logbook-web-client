package ca.ioniq.web.client.activity;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.event.note.NoteViewEvent;
import ca.ioniq.web.client.ui.note.NoteView;

public class NoteActivity extends BaseActivity implements NoteView.Presenter {
	private NoteView noteView;
	
	public NoteActivity(Place place, final ClientFactory clientFactory) {
		super(clientFactory);
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		_clientFactory.getMainMenu().setVisible(Boolean.TRUE);
		_clientFactory.getApplicationMenu().setVisible(Boolean.TRUE);
		_clientFactory.getLogoutPanel().setVisible(Boolean.TRUE);
		_clientFactory.getAdminMenu().setVisible(Boolean.FALSE);
		
		noteView = _clientFactory.getNoteView();
		
		noteView.setPresenter(this);
		panel.setWidget(noteView.asWidget());
		
		// cache domain values
		noteView.setNoteStatusList(_clientFactory.getApplicationController().getNoteStatusList());
		noteView.setPilotStatusList(_clientFactory.getApplicationController().getPilotStatusList());
		noteView.setSectorList(_clientFactory.getApplicationController().getSectorList());
		
		// get the list of notes for today and the default sector
		_clientFactory.getEventBus().fireEvent(new NoteViewEvent());
	}

	@Override
	public void goTo(Place place) {
		super.goTo(place);
	}
}
