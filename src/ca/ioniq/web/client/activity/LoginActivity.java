/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.event.login.LoginEvent;
import ca.ioniq.web.client.place.NotePlace;
import ca.ioniq.web.client.ui.ErrorMessagePanel;
import ca.ioniq.web.client.ui.login.LoginView;
import ca.ioniq.web.shared.FieldVerifier;
import ca.ioniq.web.shared.Role;
import ca.ioniq.web.shared.UserPO;

public class LoginActivity extends AbstractActivity implements LoginView.Presenter {

	private final ClientFactory clientFactory; 
	private LoginView loginView;
	
	private TextBox usernameTextBox;
	private PasswordTextBox passwordTextBox;
	private ErrorMessagePanel errorPanel;
	
	private final Label usernameErrorLabel = new Label("Le code utilisateur est obligatoire");
	private final Label passwordErrorLabel = new Label("Le mot de passe est obligatoire");
	
	public LoginActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.loginView = clientFactory.getLoginView();

		usernameTextBox = loginView.getUsernameTextBox();
		passwordTextBox = loginView.getPasswordTextBox();
		
		usernameErrorLabel.setStyleName("error-username");
		passwordErrorLabel.setStyleName("error-password");
	}
	
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		clientFactory.getMainMenu().setVisible(Boolean.FALSE);
		clientFactory.getAdminMenu().setVisible(Boolean.FALSE);
		
		loginView.setPresenter(this);
		panel.setWidget(loginView.asWidget());
		
		// clear any message
		resetErrorMessage();
		
		// remove the menu from the login screen
		clientFactory.getApplicationMenu().setVisible(Boolean.FALSE);
		clientFactory.getLogoutPanel().setVisible(Boolean.FALSE);
		
		// handler to fire validation and form submission on enter key pressed
		KeyDownHandler enterDownHandler = new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER) { login(); }
			}
		};
		
		usernameTextBox.addKeyDownHandler(enterDownHandler);
		passwordTextBox.addKeyDownHandler(enterDownHandler);
		
		loginView.getLoginButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) { login(); }
		});
		
		// remove error messages whenever any of the input fields gets focus
		FocusHandler removeErrorMessageHandler = new FocusHandler() {
			@Override
			public void onFocus(FocusEvent event) {
				resetErrorMessage();
			}
		};
		
		loginView.getUsernameTextBox().addFocusHandler(removeErrorMessageHandler);
		loginView.getPasswordTextBox().addFocusHandler(removeErrorMessageHandler);
	}
	
	private void resetErrorMessage() {
		loginView.getMainPanel().remove(usernameErrorLabel);
		loginView.getMainPanel().remove(passwordErrorLabel);
		
		if(errorPanel != null) {
			loginView.getMainPanel().remove(errorPanel);
		}
	}

	@Override
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
	
	private void login() {	
		// client side validation
		String username = usernameTextBox.getText().trim();
		String password = passwordTextBox.getText().trim();
		
		if(!FieldVerifier.isValidName(username)) {
			loginView.getMainPanel().insert(usernameErrorLabel, 2);
			return;
		} 
		if(!FieldVerifier.isValidName(password)) {
			loginView.getMainPanel().insert(passwordErrorLabel, 3);
			return;
		}
		
		StringBuilder userInfo = new StringBuilder();
		userInfo.append(URL.encode("username=")).append(URL.encode(usernameTextBox.getValue().trim()))
				.append("&")
				.append(URL.encode("password=")).append(passwordTextBox.getValue().trim());
		
		
		String url = URL.encode(BaseActivity.USER_BASE_URL + "/login");
		
		RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.POST, url);
		requestBuilder.setHeader(BaseActivity.CONTENT_TYPE, "application/x-www-form-urlencoded");
		
		try {
			requestBuilder.sendRequest(userInfo.toString(), new RequestCallback() {
				@Override
				public void onResponseReceived(Request request, Response response) {
					int responseCode = response.getStatusCode();
					if(responseCode == Response.SC_OK) {
						String contentType = response.getHeader("Content-type");
						UserPO user = parse(response.getText(), contentType);
						
						// set welcome message
						if (user != null) {
							clientFactory.getLogoutPanel().setWelcomeLabel(user.getFirstName() + " " + user.getLastName());	
							clientFactory.getApplicationController().setCurrentUser(user);
							boolean showAdminMenu = user.getRole().equals(Role.ADMIN.toString()) || user.getRole().equals(Role.DIRECTOR.toString());
							clientFactory.getApplicationMenu().toggleAdminMenu(showAdminMenu);
							clientFactory.getPlaceController().goTo(new NotePlace("view"));
						}else{
							clientFactory.getEventBus().fireEvent(new LoginEvent());
						}
						
					} else if(responseCode == Response.SC_UNAUTHORIZED) {
						errorPanel = new ErrorMessagePanel(response.getText());
						loginView.getMainPanel().add(errorPanel);
					} 
				}

				@Override
				public void onError(Request request, Throwable exception) {
					errorPanel = new ErrorMessagePanel("Error " + exception.getMessage());
					loginView.getMainPanel().add(errorPanel);
				}
			});

		} catch (RequestException e) {
			Window.alert("Failed to send the request: " + e.getMessage());
		}
		
		usernameTextBox.setText("");
		passwordTextBox.setText("");
	}
	
	private UserPO parse(String data, String contentType) {
		if(contentType.equals(BaseActivity.JSON_TYPE)) {
			return parseJson(data);
		} else {
			return parseXml(data);
		}
	}
	
	private UserPO parseJson(String data) {
		return clientFactory.getParser().deserializeSingleUser(data);
	}
	
	private UserPO parseXml(String data) {
		return new UserPO();
	}
}
