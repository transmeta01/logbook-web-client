/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client;

import ca.ioniq.web.client.place.LoginPlace;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class LogWebClient implements EntryPoint {
	private Place defaultPlace = new LoginPlace("login");
	private SimplePanel widget;

	public void onModuleLoad() {

		final ClientFactory clientFactory = GWT.create(ClientFactory.class);
//		widget = new SimplePanel((Widget)clientFactory.getLoginView());
		
		widget.getElement().setId("content");
		clientFactory.getActivityManager().setDisplay(widget);
		
		clientFactory.getPlaceHistoryHandler().register(clientFactory.getPlaceController(), clientFactory.getEventBus(), defaultPlace);
		
		clientFactory.getMainContentPanel().add(widget);
//		clientFactory.getMainContentPanel().add(new FooterPanel());

		Document document = Document.get();
		if (document != null) {
			document.setTitle("Bienvenu au logbook en ligne");
		}
		
		RootPanel.get().add(clientFactory.getMainContentPanel());
		
		// go to the place represented in the URL or else default place
		clientFactory.getPlaceHistoryHandler().handleCurrentHistory();
		
		// warm up client side cache
		clientFactory.getApplicationController().getPilotStatusList();
		clientFactory.getApplicationController().getUsersList();
		clientFactory.getApplicationController().getRoleList();
		clientFactory.getApplicationController().getSectorList();
		clientFactory.getApplicationController().getNoteStatusList();
	}
}
