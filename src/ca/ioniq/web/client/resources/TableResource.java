package ca.ioniq.web.client.resources;

//import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.CellTable;

public interface TableResource extends CellTable.Resources {
	@Source({CellTable.Style.DEFAULT_CSS, "ca/ioniq/web/client/resources/table.css"})
	TableStyle cellTableStyle();
	
//	@Source("ca/ioniq/web/client/resources/images/ajax-loader-spinner.gif")
//	ImageResource cellTableLoading();

	interface TableStyle extends CellTable.Style {}
}