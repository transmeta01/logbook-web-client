/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.ui;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.HTML;

public class ErrorMessagePanel extends Composite {
	private DecoratedPopupPanel messageBox = new DecoratedPopupPanel(true);
	
	public final static String NOT_FOUND = "The content you are looking is not available"; 
	public final static String SERVICE_UNAVAILABLE = "Serveur non disponible, veuillez contacter votre administrateur"; 
	
	public ErrorMessagePanel() {}
	
	public ErrorMessagePanel(String errorMessage) {
		HTML message = new HTML(errorMessage);
		message.setStyleName("error-login");
		messageBox.setWidget(message);
		
		// position error box at the center of the UI
//		messageBox.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
//			@Override
//			public void setPosition(int offsetWidth, int offsetHeight) {
//				int left = (Window.getClientWidth() - offsetWidth) / 5;
//				int top = (Window.getClientHeight() - offsetHeight) / 5;
//				messageBox.setPopupPosition(left, top);
//			}
//		});
		
		initWidget(messageBox);
	}
	
	public void showErrorMessage(String errorMessage) {
		messageBox.clear();
		messageBox.setWidget(new HTML(errorMessage));
		messageBox.show();
	}
	
	public void hide() {
		if(this.isVisible()) this.setVisible(Boolean.FALSE);
	}
}

