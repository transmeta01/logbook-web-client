/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.ui.pilotStatus;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.shared.PilotStatusPO;

import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.view.client.ListDataProvider;

public class PilotStatusTable extends Composite {
	
	private CellTable<PilotStatusPO> table  = new CellTable<PilotStatusPO>();
	private ListDataProvider<PilotStatusPO> dataProvider;
	
	private String STATUS_NAME_LABEL = "Nom";
	private String STATUS_DESCRIPTION_LABEL = "Description";
	private String STATUS_CODE_LABEL  = "Code";
	private static String STATUS_ACTIVE = "actif";
	
	private List<PilotStatusPO> pilotStatusList;
	
	public PilotStatusTable(ClientFactory clientFactory, CellTable.Resources tableResource) {
		
		table = new CellTable<PilotStatusPO>(100, tableResource);
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED); 
		table.setWidth("100%", true);
		
		final Column<PilotStatusPO, String> nameColumn = new TextColumn<PilotStatusPO>() {
			@Override
			public String getValue(PilotStatusPO object) {
				return object.getPilotStatusName();
			}
		};
		
		table.addColumn(nameColumn, STATUS_NAME_LABEL);
		nameColumn.setSortable(Boolean.TRUE);
		
		final Column<PilotStatusPO, String> descriptionColumn = new TextColumn<PilotStatusPO>() {
			@Override
			public String getValue(PilotStatusPO object) {
				return object.getPilotStatusDescription();
			}
		};
		
		table.addColumn(descriptionColumn, STATUS_DESCRIPTION_LABEL);
		descriptionColumn.setSortable(Boolean.TRUE);
		
		final Column<PilotStatusPO, String> codeColumn = new TextColumn<PilotStatusPO>() {
			@Override
			public String getValue(PilotStatusPO object) {
				return object.getPilotStatusCode();
			}
		};
		
		table.addColumn(codeColumn, STATUS_CODE_LABEL);
		codeColumn.setSortable(Boolean.TRUE);
		
		final Column<PilotStatusPO, String> activeColumn = new TextColumn<PilotStatusPO>() {
			@Override
			public String getValue(PilotStatusPO object) {
				return !object.isActive() ? "" : "actif";
			}
		};
		
		table.addColumn(activeColumn, STATUS_ACTIVE);
		
		// set list data
		dataProvider = new ListDataProvider<PilotStatusPO>();
		dataProvider.addDataDisplay(table);
		
		// handle sort event
		table.addColumnSortHandler(new ColumnSortEvent.Handler() {
			@SuppressWarnings("unchecked")
			@Override
			public void onColumnSort(ColumnSortEvent event) {
				Column<PilotStatusPO, ?> sortColumn = (Column<PilotStatusPO, String>) event.getColumn();
				int sortedColumnIndex = table.getColumnIndex(sortColumn); 
				setSortedColumn(sortedColumnIndex);
				
				table.getColumnSortList().push(sortColumn);
			}
		});
		
		
		initWidget(table);
	}
	
	public void addRow(PilotStatusPO newPilotStatus) {
		dataProvider.getList().clear();
		pilotStatusList.add(newPilotStatus);
		dataProvider.getList().addAll(pilotStatusList);
	}
	
	public void setPilotStatusList(List<PilotStatusPO> pilotStatusList) {
		this.pilotStatusList = pilotStatusList;
		dataProvider.getList().clear();
		dataProvider.getList().addAll(pilotStatusList);
		table.setRowCount(pilotStatusList.size(), true);
	}

	private void setSortedColumn(int sortedColumnIndex) { 
		Column<PilotStatusPO, ?> column = table.getColumn(sortedColumnIndex);
		
		if (column != null && column.isSortable()) {

			switch (sortedColumnIndex) {
			case 0:
				Collections.sort(dataProvider.getList(), new Comparator<PilotStatusPO>() {
							@Override
							public int compare(PilotStatusPO o1, PilotStatusPO o2) {
								return o1.getPilotStatusName().compareTo(o2.getPilotStatusName());
							}
						});

			case 1:
				Collections.sort(dataProvider.getList(),
						new Comparator<PilotStatusPO>() {
							@Override
							public int compare(PilotStatusPO o1, PilotStatusPO o2) {
								return o1.getPilotStatusDescription().compareTo(o2.getPilotStatusDescription());
							}
						});

			case 2:
				Collections.sort(dataProvider.getList(), new Comparator<PilotStatusPO>() {
							@Override
							public int compare(PilotStatusPO o1, PilotStatusPO o2) {
								return o1.getPilotStatusCode().compareTo(o2.getPilotStatusCode());
							}
						});
			}
		}
	}
}
