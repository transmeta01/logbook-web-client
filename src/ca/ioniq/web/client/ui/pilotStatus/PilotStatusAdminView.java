/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.ui.pilotStatus;

import java.util.List;

import ca.ioniq.web.client.ClientFactory;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;

import ca.ioniq.web.shared.PilotStatusPO;

public class PilotStatusAdminView extends Composite {
	
	private PilotStatusTable pilotStatusTable;
	private PilotStatusPO currentNew;
	
	public PilotStatusAdminView(ClientFactory clientFactory, CellTable.Resources tableResource) {
		
		FlowPanel panel = new FlowPanel();
		Anchor addStatusAnchor = new Anchor("[ajouter statut]");
		panel.add(addStatusAnchor);
		
		final NewPilotStatusDialog newPilotStatusDialog = new NewPilotStatusDialog();
		addStatusAnchor.addStyleName("new-status-anchor");
		
		addStatusAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// show widget to create new status
				newPilotStatusDialog.center();
				newPilotStatusDialog.setGlassEnabled(true);
			}
		});
		
		currentNew = new PilotStatusPO();
		
		newPilotStatusDialog.getSaveButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				// TODO validations
				
				currentNew.setPilotStatusCode(newPilotStatusDialog.getPilotStatusCodeInput().getText())
							  .setPilotStatusDescription(newPilotStatusDialog.getPilotStatusDescriptionInput().getText())
							  .setPilotStatusName(newPilotStatusDialog.getPilotStatusNameInput().getText());
				
				newPilotStatusDialog.hide();
			}
		});
		
		pilotStatusTable = new PilotStatusTable(clientFactory, tableResource);
	    
	    panel.add(pilotStatusTable);
	    initWidget(panel);
	}

	public void setPilotStatusList(List<PilotStatusPO> pilotStatusList) {
		pilotStatusTable.setPilotStatusList(pilotStatusList);
	}

	public PilotStatusPO getCurrentNew() {
		return currentNew;
	}
}