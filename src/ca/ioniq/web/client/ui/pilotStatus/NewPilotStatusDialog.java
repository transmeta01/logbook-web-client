/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.ui.pilotStatus;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

public class NewPilotStatusDialog extends DialogBox {
	
	private Button saveButton = new Button("sauvegarder");
	private Button cancelButton = new Button("annuler");
	
	private TextBox pilotStatusNameInput;
	private TextBox pilotStatusDescriptionInput;
	private TextBox pilotStatusCodeInput;
	
	public NewPilotStatusDialog() {
		
		setDialogBoxHeader("Ajout nouveau statut de pilote");
		
		final FlowPanel mainPanel = new FlowPanel();
		
		final FlowPanel buttonPanel = new FlowPanel();
		buttonPanel.add(saveButton);
		buttonPanel.add(cancelButton);
		
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// clean and hide widget
				hide();
			}
		});
		
		pilotStatusNameInput = new TextBox();		
		pilotStatusDescriptionInput = new TextBox();
		pilotStatusCodeInput = new TextBox();
		
		Grid g = new Grid(3, 2);

	    g.setWidget(0, 0, new Label("Nom: "));
	    g.setWidget(0, 1, pilotStatusNameInput);
	    g.setWidget(1, 0, new Label("Description: "));
	    g.setWidget(1, 1, pilotStatusDescriptionInput);
	    g.setWidget(2, 0, new Label("Code: "));
	    g.setWidget(2, 1, pilotStatusCodeInput);
	    	    
	    mainPanel.add(g);
		mainPanel.add(buttonPanel);
		
		setWidget(mainPanel);
	}
	
	public void setDialogBoxHeader(String header) {
		setText(header);
	}

	public Button getSaveButton() {
		return saveButton;
	}

	public Button getCancelButton() {
		return cancelButton;
	}

	public TextBox getPilotStatusNameInput() {
		return pilotStatusNameInput;
	}

	public TextBox getPilotStatusDescriptionInput() {
		return pilotStatusDescriptionInput;
	}

	public TextBox getPilotStatusCodeInput() {
		return pilotStatusCodeInput;
	}
}