package ca.ioniq.web.client.ui.audit;

import java.util.List;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.shared.AuditPO;

import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SafeHtmlHeader;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.view.client.ListDataProvider;

public class AuditTable extends Composite {

	private CellTable<AuditPO> table = new CellTable<AuditPO>();
	private ListDataProvider<AuditPO> dataProvider;
	
	private static final String TIME_LABEL = "Heure";
	private static final String DATE_LABEL = "Date";
	private static final String STATUS_LABEL = "Status";
	private static final String PILOT_STATUS_LABEL = "Status Pilote";
	private static final String NOTE_ID_LABEL = "Note #";
	private static final String MODIFIED_BY_LABEL = "Modifi&#233; par";
	private static final String NOTE_TEXT_LABEL = "Texte de la note";
	private static final String ATTACHMENT_TEXT_LABEL = "Piece jointe";
	
	public AuditTable(final ClientFactory clientFactory, CellTable.Resources tableResource) {
		FlowPanel mainPanel = new FlowPanel();
		table = new CellTable<AuditPO>(100, tableResource);
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED); 
		table.setWidth("100%", true);
		
		TextColumn<AuditPO> timeColumn = new TextColumn<AuditPO>() {
			@Override
			public String getValue(AuditPO object) {
				return object.getTime();
			}
		};
		
		table.addColumn(timeColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(TIME_LABEL)));
		
		Column<AuditPO, String> dateColumn = new TextColumn<AuditPO>() {
			@Override
			public String getValue(AuditPO object) {
				return object.getDate();
			}
		};
		
		table.addColumn(dateColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(DATE_LABEL)));
		
		Column<AuditPO, String> statusColumn = new TextColumn<AuditPO>() {
			@Override
			public String getValue(AuditPO object) {
				return object.getStatus();
			}
		};
		
		table.addColumn(statusColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(STATUS_LABEL)));
		
		Column<AuditPO, String> statusPilotColumn = new TextColumn<AuditPO>() {
			@Override
			public String getValue(AuditPO object) {
				return object.getPilotStatus();
			}
		};
		
		table.addColumn(statusPilotColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(PILOT_STATUS_LABEL)));
		
		Column<AuditPO, String> noteIdColumn = new TextColumn<AuditPO>() {
			@Override
			public String getValue(AuditPO object) {
				return object.getNoteId().toString();
			}
		};
		
		table.addColumn(noteIdColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(NOTE_ID_LABEL)));
		
		Column<AuditPO, String> modifiedByColumn = new TextColumn<AuditPO>() {
			@Override
			public String getValue(AuditPO object) {
				return object.getUsername();
			}
		};
		
		table.addColumn(modifiedByColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(MODIFIED_BY_LABEL)));
		
		Column<AuditPO, String> noteTextColumn = new TextColumn<AuditPO>() {
			@Override
			public String getValue(AuditPO object) {
				return object.getNoteText();
			}
		};
		
		table.addColumn(noteTextColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(NOTE_TEXT_LABEL)));
		
		// TODO make this column clickable
		Column<AuditPO, String> attachedDocumentColumn = new TextColumn<AuditPO>() {
			@Override
			public String getValue(AuditPO object) {
				return object.getAttachedDocument();
			}
		};
		
		table.addColumn(attachedDocumentColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(ATTACHMENT_TEXT_LABEL)));
		
		
		dataProvider = new ListDataProvider<AuditPO>();
		dataProvider.addDataDisplay(table);
		
		mainPanel.add(table);
		initWidget(mainPanel);
	}
	
	public void setAuditList(List<AuditPO> auditList) {
		dataProvider.getList().clear();
		dataProvider.getList().addAll(auditList);
		table.setRowCount(auditList.size(), true);
	}
}
