package ca.ioniq.web.client.ui.audit;

import java.util.List;

import ca.ioniq.web.shared.AuditPO;
import ca.ioniq.web.shared.RolePO;
import ca.ioniq.web.shared.SectorPO;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.IsWidget;

public interface AuditView extends IsWidget {
	
	void setPresenter(Presenter presenter);
	
	void setRoleList(List<RolePO> roleList);

	void setSectorList(List<SectorPO> sectorList);

	void setAuditList(List<AuditPO> auditList);

	public interface Presenter {
		void goTo(Place place);
	}
}

