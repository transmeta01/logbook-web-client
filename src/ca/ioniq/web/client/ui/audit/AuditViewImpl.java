package ca.ioniq.web.client.ui.audit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.activity.BaseActivity;
import ca.ioniq.web.client.event.audit.AuditEvent;
import ca.ioniq.web.client.event.audit.AuditEventHandler;
import ca.ioniq.web.client.event.audit.AuditFilter;
import ca.ioniq.web.client.event.audit.FilterAuditEvent;
import ca.ioniq.web.client.event.audit.ViewAuditBySectorEvent;
import ca.ioniq.web.client.ui.ErrorMessagePanel;
import ca.ioniq.web.shared.AuditPO;
import ca.ioniq.web.shared.RolePO;
import ca.ioniq.web.shared.SectorPO;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;

public class AuditViewImpl extends Composite implements AuditView {
	
	private ErrorMessagePanel errorMessagePanel = new ErrorMessagePanel();

	@SuppressWarnings("unused")
	private AuditView.Presenter presenter; 
	private AuditTable auditTable;
	
	private List<SectorPO> sectorValues = new ArrayList<SectorPO>();
	private ListBox sectorSelector = new ListBox();
	private SectorPO selectedSector;
	
	// sector map name to index
	private Map<String, Integer> sectorNameIndexMap = new HashMap<String, Integer>();
	
	
	private DateBox startDateSelector;
	private Date startDate;
	
	private DateBox endDateSelector;
	private Date endDate;
	
	private Date today = new Date();
	
	private List<RolePO> roleValues = new ArrayList<RolePO>();
	private ListBox roleSelector = new ListBox();
	private RolePO selectedRole;
	
	private TextBox keywordInput = new TextBox();
	
	private ClientFactory clientFactory;
	
	private List<AuditPO> currentAuditList;
	
	private AuditFilter auditFilter = new AuditFilter();
	
	public AuditViewImpl(final ClientFactory _clientFactory, CellTable.Resources tableResource) {
		clientFactory = _clientFactory;
		
		FlowPanel mainPanel = new FlowPanel();
		
		auditTable = new AuditTable(_clientFactory, tableResource);
		
		FlowPanel filterWidgetPanel = new FlowPanel();
		filterWidgetPanel.getElement().setId("filter-panel");
		
		FlowPanel sectorFilterPanel = new FlowPanel();
		sectorFilterPanel.addStyleName("inline");
		
		sectorFilterPanel.add(new Label("Secteur: "));
		sectorFilterPanel.add(sectorSelector);
		filterWidgetPanel.add(sectorFilterPanel);
		sectorSelector.setStyleName("selector");
		
		FlowPanel startDateFilterPanel = new FlowPanel();
		startDateFilterPanel.addStyleName("inline");
		startDateSelector = new DateBox();
		startDateSelector.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd MMMM yyyy")));

		startDateFilterPanel.add(new Label("date début: "));
		startDateFilterPanel.add(startDateSelector);
		filterWidgetPanel.add(startDateFilterPanel);
		
		FlowPanel endDateFilterpanel = new FlowPanel();
		endDateFilterpanel.addStyleName("inline");
		endDateSelector = new DateBox();
		endDateSelector.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd MMMM yyyy")));
		
		endDateFilterpanel.add(new Label("Date fin: "));
		endDateFilterpanel.add(endDateSelector);
		filterWidgetPanel.add(endDateFilterpanel);
		
		FlowPanel roleFilterPanel = new FlowPanel();
		roleFilterPanel.addStyleName("inline");
		
		
		roleFilterPanel.add(new Label("Role: "));
		roleFilterPanel.add(roleSelector);
		filterWidgetPanel.add(roleFilterPanel);
		
		FlowPanel keywordFilterPanel = new FlowPanel();
		keywordFilterPanel.addStyleName("inline");
		keywordFilterPanel.add(new Label("Mots clés: "));
		keywordFilterPanel.add(keywordInput);
		filterWidgetPanel.add(keywordFilterPanel);
		
		// register change and click handlers
		sectorSelector.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				selectedSector = sectorValues.get(sectorSelector.getSelectedIndex());
				startDateSelector.getTextBox().setText("");
				endDateSelector.getTextBox().setText("");
				
				_clientFactory.getEventBus().fireEvent(new ViewAuditBySectorEvent(selectedSector.getSectorName()));
				_clientFactory.getSectorMenu().setActive(selectedSector.getSectorName());
			}
		});
		
		startDateSelector.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				startDate = event.getValue();
				filter(startDate, null, null, null);
			}
		});
		
		endDateSelector.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				endDate = event.getValue();
				filter(null, endDate, null, null);
			}
		});
		
		roleSelector.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				selectedRole = roleValues.get(roleSelector.getSelectedIndex());
				filter(null, null, selectedRole.getRoleName(), null);
			}
		});
		
		keywordInput.addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				String keywords = keywordInput.getText().trim();
				filter(null, null, null, keywords);
				
			}
		});
		
		keywordInput.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getCharCode() == KeyCodes.KEY_ENTER) {
					String keywords = keywordInput.getText().trim();
					filter(null, null, null, keywords);
				}
			}
		});
		
		keywordInput.addFocusHandler(new FocusHandler() {
			@Override
			public void onFocus(FocusEvent event) {
				keywordInput.setText("");
			}
		});
		
		mainPanel.add(filterWidgetPanel);
		mainPanel.add(auditTable);
		
		bind();
		
		initWidget(mainPanel);
	}
	
	private void bind() {
		
		clientFactory.getEventBus().addHandler(AuditEvent.TYPE, new AuditEventHandler() {
			@Override
			public void onViewAuditBySector(ViewAuditBySectorEvent event) {
				String url = URL.encode(BaseActivity.AUDIT_BASE_URL + "/sector/" + event.getSectorname());
				RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
				
				Integer selectedIndex = sectorNameIndexMap.get(event.getSectorname());
				sectorSelector.setSelectedIndex(selectedIndex);
				
				try {
					requestBuilder.sendRequest(null, new AuditRequestCallback());
				} catch (RequestException e) {
					errorMessagePanel.showErrorMessage(ErrorMessagePanel.SERVICE_UNAVAILABLE);
				}
			}
			
			@Override
			public void onFilterAudit(FilterAuditEvent event) {
				setAuditList(auditFilter.filter(currentAuditList, event.getStartDate(), event.getEndDate(), event.getRole(), event.getKeywords()));
			}
		});
	}
	
	private void filter(Date start, Date end, String role, String keywords) {
		if(start == null && startDateSelector.getValue() == null) {
			start = today;
			startDateSelector.setValue(start);
		} else {
			start = startDateSelector.getValue();
		}
		
		if(end == null && endDateSelector.getValue() == null) {
			end = today;
			endDateSelector.setValue(end);
		} else {
			end = endDateSelector.getValue();
		}
		
		role = role == null ? roleValues.get(roleSelector.getSelectedIndex()).getRoleName() : role;
		keywords = keywords == null ? keywordInput.getText() : keywords;
		
		clientFactory.getEventBus().fireEvent(new FilterAuditEvent(start, end, role, keywords));
	}
	
	private void processResponse(String json) {
		keywordInput.setText("");
		roleSelector.setSelectedIndex(0);
		currentAuditList = clientFactory.getParser().deserializeAuditList(json);
		setAuditList(currentAuditList);
	}
	
	@Override
	public void setPresenter(Presenter presenter) {	}
	
	@Override
	public void setAuditList(List<AuditPO> auditList) {
		auditTable.setAuditList(auditList);
	}
	
	public void setSectorList(List<SectorPO> sectorList) {
		if (sectorValues.isEmpty()) {
			int index = 0;
			for (SectorPO sector : sectorList) {
				sectorValues.add(sector);
				sectorSelector.addItem(sector.getSectorName());
				sectorNameIndexMap.put(sector.getSectorName(), index);
				index++;
			}
		}
	}
	
	public void setRoleList(List<RolePO> roleList) {
		if (roleValues.isEmpty()) {
			for (RolePO role : roleList) {
				roleValues.add(role);
				roleSelector.addItem(role.getRoleName());
			}
		}
	}

	public SectorPO getSelectedSector() {
		return selectedSector;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public RolePO getSelectedRole() {
		return selectedRole;
	}

	public TextBox getKeywordInput() {
		return keywordInput;
	}
	
	/*
	 *  
	 */
	private class AuditRequestCallback implements RequestCallback {
		@Override
		public void onResponseReceived(Request request, Response response) {
			int responseCode = response.getStatusCode();
			if (responseCode == Response.SC_OK) {
				String json = response.getText();
				processResponse(json);
			}
		}

		@Override
		public void onError(Request request, Throwable exception) {
			errorMessagePanel.showErrorMessage(exception.getMessage());
		}
	}
}
