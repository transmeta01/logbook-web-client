/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.ui.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ioniq.web.shared.RolePO;
import ca.ioniq.web.shared.UserPO;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

public class UserEditDialog extends DialogBox {
	private TextBox usernameInput = new TextBox();
	private TextBox lastnameInput = new TextBox();
	private TextBox firstnameInput = new TextBox();
	private TextBox passwordInput = new TextBox();
	private TextBox confirmPasswordInput = new TextBox();
	
	private ListBox roleSelector = new ListBox();
	
	private Button saveButton = new Button("sauvegarder");
	private Button cancelButton = new Button("annuler");
	private CheckBox delete = new CheckBox("Effacer");
	
	private UserPO editUser;
	
	private Map<String, Integer> roleNameIndexMap = new HashMap<String, Integer>();
	
	public UserEditDialog() {
		
		FlowPanel mainPanel = new FlowPanel();
		
		mainPanel.setStyleName("edit-user-dialog");
				
		Grid g = new Grid(6, 2);

	    g.setWidget(0, 0, new Label("Code utilisateur:"));
	    g.setWidget(0, 1, usernameInput);
	    g.setWidget(1, 0, new Label("Nom:"));
	    g.setWidget(1, 1, lastnameInput);
	    g.setWidget(2, 0, new Label("Pr\u00E9nom:"));
	    g.setWidget(2, 1, firstnameInput);
	    g.setWidget(3, 0, new Label("Mot de passe:"));
	    g.setWidget(3, 1, passwordInput);
	    g.setWidget(4, 0, new Label("Confirmer mot de passe:"));
	    g.setWidget(4, 1, confirmPasswordInput);
	    g.setWidget(5, 0, new Label("R\u00f4le:"));
	    g.setWidget(5, 1, roleSelector);
	    
	    
	    // You can use the CellFormatter to affect the layout of the grid's cells.
	    g.getCellFormatter().setWidth(0, 1, "256px");

	    mainPanel.add(g);
	    /*
		FlowPanel labelPanel = new FlowPanel();
		labelPanel.setStyleName("user-dialog-left");
        
		FlowPanel inputPanel = new FlowPanel();
		inputPanel.setStyleName("user-dialog-right");
		
		labelPanel.add(new Label("Code utilisateur:"));
		inputPanel.add(usernameInput);
		
		labelPanel.add(new Label("Nom:"));
		inputPanel.add(lastnameInput);
		
		labelPanel.add(new Label("Pr\u00E9nom:"));
		inputPanel.add(firstnameInput);
		
		labelPanel.add(new Label("Mot de passe:"));
		inputPanel.add(passwordInput);
		
		labelPanel.add(new Label("Confirmer mot de passe:"));
		inputPanel.add(confirmPasswordInput);
		
		labelPanel.add(new Label("Rle"));
		inputPanel.add(roleSelector);
										
		mainPanel.add(labelPanel);
		mainPanel.add(inputPanel);
		/*
		 * 
		 */
		FlowPanel buttonPanel = new FlowPanel();
		
		buttonPanel.add(saveButton);
		buttonPanel.add(cancelButton);
		buttonPanel.add(delete);
		buttonPanel.getElement().setId("dialog-button-panel");
		
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				hide();
			}
		});
		
		mainPanel.add(buttonPanel);
		setWidget(mainPanel);
		
		setDialogBoxHeader("Nouvel utilisateur");
	}
	
	public void setEditingUser(UserPO user) {
		editUser = user;
		setDialogBoxHeader("Modification info utilisateur");
		setAnimationEnabled(true);
		setGlassEnabled(true);
	}
	
	public void setDialogBoxHeader(String text) {
		this.setText(text);
	}
	
	public void setRoleList(List<RolePO> roleList) {
		int index = 0;
		for (RolePO role : roleList) {
			roleSelector.addItem(role.getRoleName());
			roleNameIndexMap.put(role.getRoleName(), index);
			index++;
		}
	}
	
	public void reset() {
		usernameInput.setText("");
		lastnameInput.setText("");
		firstnameInput.setText("");
		passwordInput.setText("");
		confirmPasswordInput.setText("");
		roleSelector.setSelectedIndex(0);
		
		editUser = null;
		
		center();
	}
	
	public Button getSaveButton() { return this.saveButton; }
	public CheckBox getDeleteCheckBox() { return delete; }
	public UserPO getEditUser() { return this.editUser; }

	public void setUser(UserPO selectedUser) {
		editUser = selectedUser;
		usernameInput.setText(selectedUser.getUserName());
		lastnameInput.setText(selectedUser.getLastName());
		firstnameInput.setText(selectedUser.getFirstName());
		int index = roleNameIndexMap.get(selectedUser.getRole());
		roleSelector.setSelectedIndex(index);
	}
}
