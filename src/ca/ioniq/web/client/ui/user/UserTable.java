/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.ui.user;

import java.util.List;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.shared.UserPO;

import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SafeHtmlHeader;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;

public class UserTable extends Composite {
	private CellTable<UserPO> table = new CellTable<UserPO>();
	private ListDataProvider<UserPO> dataProvider;
	
	private static final String USERNAME_LABEL = "Code utilisateur";
	private static final String LAST_NAME_LABEL = "Nom";
	private static final String FIRST_NAME_LABEL = "Pr\u00E9nom";
	private static final String ROLE_LABEL = "R\u00f4le";
	private static final String ACTIVE_LABEL = "actif";
	
	public UserTable(final ClientFactory clientFactory, CellTable.Resources tableResource) {
		
		FlowPanel mainPanel = new FlowPanel();
		table = new CellTable<UserPO>(100, tableResource);
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED); 
		table.setWidth("100%", true);
		
		Column<UserPO, String> userNameColumn = new TextColumn<UserPO>() {
			@Override
			public String getValue(UserPO object) {
				return object.getUserName();
			}
		};
		
		table.addColumn(userNameColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(USERNAME_LABEL)));
		
		Column<UserPO, String> lastNameColumn = new TextColumn<UserPO>() {
			@Override
			public String getValue(UserPO object) {
				return object.getLastName();
			}
		};
		
		table.addColumn(lastNameColumn, new SafeHtmlHeader(SafeHtmlUtils.fromSafeConstant(LAST_NAME_LABEL)));
		
		Column<UserPO, String> firstNameColumn = new TextColumn<UserPO>() {
			@Override
			public String getValue(UserPO object) {
				return object.getFirstName();
			}
		};
		
		table.addColumn(firstNameColumn, FIRST_NAME_LABEL);
		
		Column<UserPO, String> roleColumn = new TextColumn<UserPO>() {
			@Override
			public String getValue(UserPO object) {
				return object.getRole();
			}
		};
		
		table.addColumn(roleColumn, ROLE_LABEL);
		
		Column<UserPO, String> activeColumn = new TextColumn<UserPO>() {
			@Override
			public String getValue(UserPO object) {
				return !object.isActive() ? "desactif" : "actif";
			}
		};
		
		table.addColumn(activeColumn, ACTIVE_LABEL);
		
		dataProvider = new ListDataProvider<UserPO>();
		dataProvider.addDataDisplay(table);
		
		mainPanel.add(table);
		initWidget(mainPanel);
	}
	
	public void setUserList(List<UserPO> userList) {
		dataProvider.getList().clear();
		dataProvider.getList().addAll(userList);
		table.setRowCount(userList.size(), true);
	}

	public void setSelectionHandler(SelectionModel<UserPO> selectionModel) {
		table.setSelectionModel(selectionModel);
	}
}
