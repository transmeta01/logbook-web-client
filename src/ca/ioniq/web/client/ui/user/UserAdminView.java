/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.ui.user;

import java.util.List;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.shared.RolePO;
import ca.ioniq.web.shared.UserPO;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionModel;

public class UserAdminView extends Composite {
	
	private UserTable userTable;
	private UserEditDialog userEditDialog;
	
	private Anchor createNewUser = new Anchor("[ajouter utilisateur]");
	
	private Boolean editMode = Boolean.TRUE;
	
	public UserAdminView(final ClientFactory clientFactory, CellTable.Resources tableResource) {
		
		FlowPanel mainPanel = new FlowPanel();

		userEditDialog = new UserEditDialog();
		userEditDialog.setDialogBoxHeader("Cr\u00E9ation d'un nouveau profile utilisateur");
		userEditDialog.hide();
		
		userTable = new UserTable(clientFactory, tableResource);
		
		final SelectionModel<UserPO> selectionModel = new NoSelectionModel<UserPO>();
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				userEditDialog.setDialogBoxHeader("Modification du profile utilisateur");
				UserPO selected = ((NoSelectionModel<UserPO>) selectionModel).getLastSelectedObject();
				userEditDialog.setUser(selected);
				userEditDialog.center();
//				clientFactory.getEventBus().fireEvent(new AdminUserEditEvent(selected));
			}
		});
		
		userTable.setSelectionHandler(selectionModel);
		createNewUser.addStyleName("new-user-anchor");
		
		mainPanel.add(createNewUser);
		mainPanel.add(userTable);
		
		createNewUser.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				editMode = false;
				userEditDialog.reset();
				userEditDialog.setGlassEnabled(true);
			}
		});
		
		userEditDialog.getSaveButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(editMode) {
					userEditDialog.setDialogBoxHeader("Modification du profile utilisateur");
					
				}
				
				// TODO fire event to persist to server 
				
				userEditDialog.setDialogBoxHeader("Modification du profile utilisateur");
				
				// when done
				userEditDialog.reset();
				userEditDialog.hide();
			}
		});
		
		userEditDialog.getDeleteCheckBox().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				boolean checked = ((CheckBox) event.getSource()).getValue();
				if(checked) {
					userEditDialog.getSaveButton().setText("Effacer");
				} else {
					userEditDialog.getSaveButton().setText("Sauvegarder");
				}
			}
		});
		
		initWidget(mainPanel);
	}

	public void setUserList(List<UserPO> userList) {
		userTable.setUserList(userList);
	}

	public void setRoleList(List<RolePO> roleList) {
		userEditDialog.setRoleList(roleList);
	}
}