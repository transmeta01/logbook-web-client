package ca.ioniq.web.client.ui.admin;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.ui.BasePanel;
import ca.ioniq.web.client.ui.pilotStatus.PilotStatusAdminView;
import ca.ioniq.web.client.ui.user.UserAdminView;
import ca.ioniq.web.shared.PilotStatusPO;
import ca.ioniq.web.shared.RolePO;
import ca.ioniq.web.shared.UserPO;

public class AdminViewImpl extends BasePanel implements AdminView {

	private ClientFactory clientFactory;
	
	private final UserAdminView userAdminView;
	private final PilotStatusAdminView pilotStatusAdminView;
	
	public AdminViewImpl(final ClientFactory clientFactory) {
		super(clientFactory);
		
		// TODO i18n this
		final Anchor userAdminAnchor = new Anchor("Gestion des utilisateurs");
		final Anchor pilotStatusAnchor = new Anchor("Gestion des statuts pilote");
		userAdminAnchor.setStyleName("admin-menu-button");
		pilotStatusAnchor.setStyleName("admin-menu-button");
		
		FlowPanel anchorPanel = new FlowPanel();
		anchorPanel.add(userAdminAnchor);
		anchorPanel.add(pilotStatusAnchor);
		
		anchorPanel.getElement().setId("admin-menu");
		
		userAdminView = clientFactory.getUserAdminView();
		pilotStatusAdminView = clientFactory.getPilotStatusAdminView();
		
		final FlowPanel mainPanel = new FlowPanel();
		mainPanel.add(anchorPanel);
		
		// show user admin view first by default
		userAdminAnchor.addStyleName("active");
		mainPanel.add(userAdminView);
		
		userAdminAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				mainPanel.remove(pilotStatusAdminView);
				mainPanel.add(userAdminView);
				userAdminAnchor.addStyleName("active");
				pilotStatusAnchor.removeStyleName("active");
			}
		});
		
		pilotStatusAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				mainPanel.remove(userAdminView);
				mainPanel.add(pilotStatusAdminView);
				pilotStatusAnchor.addStyleName("active");
				userAdminAnchor.removeStyleName("active");
			}
		});
		
		initWidget(mainPanel);
	}

	@Override
	public void setPilotStatusList(List<PilotStatusPO> pilotStatusList) {
		pilotStatusAdminView.setPilotStatusList(clientFactory.getApplicationController().getPilotStatusList());
	}

	@Override
	public void setUserList(List<UserPO> userList) {
		userAdminView.setUserList(clientFactory.getApplicationController().getUsersList());
	}
	
	@Override
	public void setRoleList(List<RolePO> roleList) {
		userAdminView.setRoleList(clientFactory.getApplicationController().getRoleList());
	}
}