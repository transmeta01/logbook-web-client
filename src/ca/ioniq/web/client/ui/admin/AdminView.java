package ca.ioniq.web.client.ui.admin;

import java.util.List;

import ca.ioniq.web.shared.PilotStatusPO;
import ca.ioniq.web.shared.RolePO;
import ca.ioniq.web.shared.UserPO;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.IsWidget;

public interface AdminView extends IsWidget {
	
	void setPilotStatusList(List<PilotStatusPO> pilotStatusList);
	void setUserList(List<UserPO> userList);
	void setRoleList(List<RolePO> roleList);
	
//	void setPresenter(Presenter presenter);
	
	public interface Presenter {
		void goTo(Place place);
	}
}
