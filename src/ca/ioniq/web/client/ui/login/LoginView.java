package ca.ioniq.web.client.ui.login;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;

public interface LoginView extends IsWidget {
	
	public Button getLoginButton() ;

	public FlowPanel getMainPanel();

	public TextBox getUsernameTextBox();
	
	public void setPresenter(Presenter presenter);

	public PasswordTextBox getPasswordTextBox();
	
	public interface Presenter {
		void goTo(Place place);
	}
}
