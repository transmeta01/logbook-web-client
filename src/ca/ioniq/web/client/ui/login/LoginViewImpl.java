package ca.ioniq.web.client.ui.login;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;

public class LoginViewImpl extends Composite implements LoginView {

	public static final String PRIMARY_STYLE = "field";
	public static final String SECONDARY_STYLE = "required";
	public static final String USER_INPUT_STYLE = "user-input";
	
	private Label loginGreeting = new Label("Connexion");
	private Label nameLabel = new Label("Code utilisateur:");
	private Label passwordLabel = new Label("Mot de passe:");
	
	private TextBox usernameTextBox = new TextBox();
	private PasswordTextBox passwordTextBox = new PasswordTextBox();
	
	private Button loginButton = new Button("Login");
	
	final FlowPanel mainPanel = new FlowPanel();
	
	@SuppressWarnings("unused")
	private LoginView.Presenter presenter;
	
	public LoginViewImpl() {
		mainPanel.getElement().setId("login");
		
		loginGreeting.setStyleName("login-header");
		
		mainPanel.add(loginGreeting);
		
		Grid g = new Grid(2, 2);

	    g.setWidget(0, 0, nameLabel);
	    g.setWidget(0, 1, usernameTextBox );
	    g.setWidget(1, 0, passwordLabel);
	    g.setWidget(1, 1, passwordTextBox);
	      
	    //g.getCellFormatter().setWidth(0, 0, "150px");
	    
		usernameTextBox.setFocus(Boolean.TRUE);
		usernameTextBox.setStylePrimaryName(PRIMARY_STYLE);
		usernameTextBox.addStyleName(SECONDARY_STYLE);
		
		passwordTextBox.setStylePrimaryName(PRIMARY_STYLE);
		passwordTextBox.addStyleName(SECONDARY_STYLE);
		
		mainPanel.add(g);
		
		mainPanel.add(loginButton);
		
		usernameTextBox.setCursorPos(usernameTextBox.getText().length());
		usernameTextBox.setFocus(true);
		
		initWidget(mainPanel);
		
//		presenter.goTo(new AdminPlace());
	}
	
	@Override
	public Button getLoginButton() {
		return loginButton;
	}

	@Override
	public FlowPanel getMainPanel() {
		return mainPanel;
	}

	@Override
	public TextBox getUsernameTextBox() {
		return usernameTextBox;
	}

	@Override
	public PasswordTextBox getPasswordTextBox() {
		return passwordTextBox;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
}
