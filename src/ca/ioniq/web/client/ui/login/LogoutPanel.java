package ca.ioniq.web.client.ui.login;

import java.util.ArrayList;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.place.LoginPlace;
import ca.ioniq.web.shared.NotePO;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;

public class LogoutPanel extends FlowPanel {
	
	private Label usernameLabel;
	
	public LogoutPanel(final ClientFactory clientFactory) {
		usernameLabel = new Label("");
		Anchor logoutLink = new Anchor("Fermeture de session");
		add(usernameLabel);
		add(logoutLink);
		
		getElement().setId("logout-panel");
		
		logoutLink.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// TODO invalidate all tokens and clean cache
				clientFactory.getApplicationController().setCurrentUser(null);
				clientFactory.getApplicationController().setNoteList(new ArrayList<NotePO>());
				clientFactory.getPlaceController().goTo(new LoginPlace("login"));
			}
		});
	}
	
	public void setWelcomeLabel(String text) {
		String welcome = "Bienvenue ";
		usernameLabel.setText(welcome + text);
	}
}
