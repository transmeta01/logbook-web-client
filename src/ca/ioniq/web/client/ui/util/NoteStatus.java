package ca.ioniq.web.client.ui.util;

public enum NoteStatus {
	TRAITER {
		public String toString() {
			return "traiter";
		}
	}, 
	
	EN_RETARD {
		public String toString() {
			return "retard";
		}
	}, 
	
	CANCELLER {
		public String toString() {
			return "canceller";
		}
	}, 
	
	ATTENTE {
		public String toString() {
			return "attente";
		}
	}; 
	
	public static NoteStatus find(String name) {
		for(NoteStatus noteStatus : NoteStatus.values()) {
			if(noteStatus.toString().equals(name)) {
				return noteStatus;
			}
		}
		
		return null;
	}
}