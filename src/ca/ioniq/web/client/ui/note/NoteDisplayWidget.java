package ca.ioniq.web.client.ui.note;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.i18n.client.DateTimeFormat;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.activity.BaseActivity;
import ca.ioniq.web.client.event.attachment.ViewAttachmentEvent;
import ca.ioniq.web.client.event.note.NoteEditEvent;
import ca.ioniq.web.client.ui.util.NoteStatus;
import ca.ioniq.web.shared.AttachmentPO;
import ca.ioniq.web.shared.NotePO;
import ca.ioniq.web.shared.NoteStatusPO;
import ca.ioniq.web.shared.PilotStatusPO;

public class NoteDisplayWidget extends Composite {
	
	private List<AttachmentPO> attachmentList;
	
	private Map<Anchor, AttachmentPO> attachemntMap = new HashMap<Anchor, AttachmentPO>();
	
	private String ATTACHMENT_TITLE = "Ajouter une piece jointe";
	
	private Anchor addAttachmentAnchor = new Anchor(ATTACHMENT_TITLE);
	private Anchor noteStatusAnchor;
	
	private Label noteText;
	
	private NoteEditDialog editDialog;
	
	private HorizontalPanel noteHeader = new HorizontalPanel();
	
	private NotePO encapsulatedNote;
	
	private String NOTE_INTERACTION = "note-interaction";
	private String NOTE_DISPLAY_FIELD = "note-display-field";
	
	private Label noteTime;
	private Label notePilotStatus;
	private Anchor noteEditAnchor;
	
	private ClientFactory clientFactory;
	
	private NoteStatusPO doneStatus;
	
	// note footer panel
	private FlowPanel noteFooter = new FlowPanel();
	
	public NoteDisplayWidget(final ClientFactory _clientFactory, final NotePO note) {
		
		clientFactory = _clientFactory;
		
		doneStatus = _clientFactory.getApplicationController().getNoteStatusEmMap().get(NoteStatus.TRAITER);
		
		encapsulatedNote = note;
		
		// header panel
		noteHeader = new HorizontalPanel();
		noteHeader.getElement().setId("note-header");
		noteHeader.setWidth("100%");

		noteEditAnchor = new Anchor("\u00E9diter");
		noteEditAnchor.setStyleName(NOTE_INTERACTION);
		noteHeader.setCellWidth(noteEditAnchor,"25%");
				
		noteTime = new Label("Heure: " + note.getNoteTime());
		noteTime.setStyleName(NOTE_DISPLAY_FIELD);
		noteHeader.setCellWidth(noteTime,"25%");
		noteHeader.add(noteTime);
		
		notePilotStatus = new Label("Statut: " + clientFactory.getApplicationController().getPilotStatusMap().get(note.getPilotStatusId()).getPilotStatusName());
		notePilotStatus.setStyleName(NOTE_DISPLAY_FIELD);
		noteHeader.setCellWidth(notePilotStatus,"25%");
		noteHeader.add(notePilotStatus);
		
		editDialog = clientFactory.getNoteEditDialog();
		editDialog.hide(true);
		
		final ClickHandler noteHeaderHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				encapsulatedNote.setNoteStatusId(clientFactory.getApplicationController().getNoteStatusEmMap().get(NoteStatus.TRAITER).getNoteStatusId());
				editDialog.setDialogBoxHeader("Modification de note");
				editDialog.setCurrentNote(encapsulatedNote);
				editDialog.center();
				editDialog.setGlassEnabled(true);
			}
		};
		
		noteEditAnchor.addClickHandler(noteHeaderHandler);
		
		if(note.getNoteStatusId() != doneStatus.getNoteStatusId()) {
			NoteStatusPO noteStatusPo = clientFactory.getApplicationController().getNoteStatusEmMap().get(NoteStatus.TRAITER);
			
			noteStatusAnchor = new Anchor(noteStatusPo.getNoteStatusName());
			noteStatusAnchor.setStyleName(NOTE_INTERACTION);
			
			noteHeader.add(noteEditAnchor);
			noteHeader.add(noteStatusAnchor);
			
			/* 
			 * bind the click handler for the note status anchor
			 *
			 * when this anchor it clicked, the appropriate colour should be applied to 
			 * the noteText label to reflect the new status. 
			 * 
			 * The anchor should be removed after it note status is changed to "traiter"
			 */
			noteStatusAnchor.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					noteStatusAnchor.setVisible(Boolean.FALSE);
					noteHeader.remove(noteStatusAnchor);
					noteText.addStyleName("green");
					
					// call server to update status
					NoteStatusPO noteStatus = clientFactory.getApplicationController().getNoteStatusEmMap().get(NoteStatus.TRAITER);
					Long noteStatusId = noteStatus.getNoteStatusId();
					encapsulatedNote.setNoteStatusId(noteStatusId);
					setDisplayNoteColor(encapsulatedNote);
					
					noteEditAnchor.setVisible(Boolean.FALSE);
					
					clientFactory.getEventBus().fireEvent(new NoteEditEvent(encapsulatedNote));
				}
			});
		}
		
		// note text edit mode panel
		final TextArea noteEditTextArea = new TextArea();
		noteEditTextArea.setCharacterWidth(160);
		noteEditTextArea.setVisibleLines(4);
		
		// middle panel
		final FlowPanel noteTextPanel = new FlowPanel();
		noteText = new Label(note.getNoteText());
		noteText.setStyleName("note-text");
		noteTextPanel.add(noteText);
		
		// determine the note display colour code
		setDisplayNoteColor(note);
		
		Label lastModifiedBy = new Label(note.getUsername() + " " + note.getLastModified());
		lastModifiedBy.setStyleName("note-metadata");
				
		// add attachment, on click show file upload pop-up
		ClickHandler addAttachmentHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Long userId = _clientFactory.getApplicationController().getCurrentUser().getId();
				FileUploadDialogBox uploadBox = new FileUploadDialogBox(_clientFactory, note, userId);
				uploadBox.center();
			}
		};
		
		addAttachmentAnchor.addClickHandler(addAttachmentHandler);
		
		addAttachmentAnchor.setStyleName("add-attachment-anchor");
		noteFooter.add(addAttachmentAnchor);
		noteFooter.setStyleName("note-footer");
		
		// get the clicked anchor and retrieve the associated attachment id
		ClickHandler viewAttachmentClickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Anchor clicked = (Anchor)event.getSource();
				Long selectedId = attachemntMap.get(clicked).getId();
				clientFactory.getEventBus().fireEvent(new ViewAttachmentEvent(selectedId));
			}
		};
		
		if(note.getAttachmentList() != null && !note.getAttachmentList().isEmpty()) {
			FlowPanel attachmentPanel = new FlowPanel();
			
			if(note.getNoteStatusId() == doneStatus.getNoteStatusId()) {
				addAttachmentAnchor.setVisible(false);
			}
			
			for (AttachmentPO attachment : note.getAttachmentList()) {
				String target = BaseActivity.BASE_URL + "rest/note/attachment/" + attachment.getId();
				Anchor attachmentLink = new Anchor(attachment.getDocumentName(), target);
				attachmentLink.setStyleName("attachment-anchor");
				attachmentLink.addClickHandler(viewAttachmentClickHandler);

				attachemntMap.put(attachmentLink, attachment);
				attachmentPanel.add(attachmentLink);
			}

			attachmentPanel.setStyleName("attachment-panel");
			noteFooter.add(attachmentPanel);
		}
		
		noteFooter.add(lastModifiedBy);
		
		FlowPanel panel = new FlowPanel();
		panel.add(noteHeader);
		panel.add(noteTextPanel);
		panel.add(noteFooter);
		
		initWidget(panel);
		setStyleName("note-display");
	}

	public void updateWidget(NotePO updatedNote) {
		noteText.setText(updatedNote.getNoteText());
		PilotStatusPO pilotStatus = clientFactory.getApplicationController().getPilotStatusMap().get(updatedNote.getPilotStatusId());
		notePilotStatus.setText(pilotStatus.getPilotStatusName());
		noteTime.setText(updatedNote.getNoteTime());

		setDisplayNoteColor(updatedNote);
	}
	
	/**
	 * determine which status colour code the note should be in base on the note attributes :
	 * -status id 
	 * -note date and 
	 * -the time of
	 * 
	 * @param noteToEvaluate
	 */
	void setDisplayNoteColor(NotePO noteToEvaluate) {
		Date today = new Date();
		int currentTime =  new Integer(DateTimeFormat.getFormat("Hmm").format(today));
		int noteTime = new Integer(noteToEvaluate.getNoteTime().replace(":", ""));
		
		boolean isBefore = clientFactory.isBefore(noteToEvaluate.getNoteDate(), today);
		boolean sameDay = clientFactory.isSameDay(noteToEvaluate.getNoteDate(), today);
		
		if(noteToEvaluate.getNoteStatusId() == doneStatus.getNoteStatusId()) {
			noteText.addStyleName("green");
			addAttachmentAnchor.setVisible(false);
		 } else if(isBefore || (sameDay && noteTime < currentTime)) {
			 noteText.addStyleName("yellow");
		 } else {
			 noteText.addStyleName("white");
		}
	}
	
	public NotePO getNote() {
		return encapsulatedNote;
	}
	
	public NoteDisplayWidget getNoteDisplay() {
		return this;
	}

	public List<AttachmentPO> getAttachmentList() {
		return attachmentList;
	}

	public NoteDisplayWidget setAttachmentList(List<AttachmentPO> attachmentList) {
		this.attachmentList = attachmentList;
		return this;
	}
}

