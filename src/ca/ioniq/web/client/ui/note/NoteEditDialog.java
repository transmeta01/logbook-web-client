package ca.ioniq.web.client.ui.note;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.ui.BasePanel;
import ca.ioniq.web.client.ui.util.NoteStatus;
import ca.ioniq.web.shared.NotePO;
import ca.ioniq.web.shared.NoteStatusPO;
import ca.ioniq.web.shared.PilotStatusPO;
import ca.ioniq.web.shared.SectorPO;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;

public class NoteEditDialog extends DialogBox {
	private Button saveButton = new Button("sauvegarder");
	private Button cancelButton = new Button("annuler");
	
	private NoteEditTextArea noteEditTextArea = new NoteEditTextArea();
	
	private ListBox sectorSelector = new ListBox();
	private List<SectorPO> sectorValues = new ArrayList<SectorPO>();
	
	private Long selectedSectorId;
	
	private ListBox pilotStatusSelector = new ListBox();
	private List<PilotStatusPO> pilotStatusValues = new ArrayList<PilotStatusPO>();
	
	private Map<NoteStatus, NoteStatusPO> noteStatusMap = new HashMap<NoteStatus, NoteStatusPO>();
	
	private Long selectedPilotStatusId;
	
	private String selectedTime;
	private TextBox timeSelector;
	
	private DateBox dateSelector;
	private Date selectedDate;
	
	private Label timeLabel = new Label("Heure:");
	
	private NotePO note;
	
	private ClientFactory _clientFactory;
	
	private Label errorMessagelabel = new Label();
		
	public NoteEditDialog(final ClientFactory clientFactory) {
		_clientFactory = clientFactory;
		
		final FlowPanel mainPanel = new FlowPanel(); 
		
		Grid mainGrid = new Grid(1, 2);
		Grid left = new Grid(2, 2);
		Grid right = new Grid(2, 2);
		
		mainGrid.setWidget(0, 0, left);
		mainGrid.setWidget(0, 1, right);
		
		timeSelector = new TextBox();
		timeSelector.setStyleName("note-input");
		timeSelector.setVisibleLength(5);
		timeSelector.setText(BasePanel.timeFormat.format(new Date()));
		
		left.setWidget(0, 0, timeLabel);
		left.setWidget(0, 1, timeSelector);
		
		// on focus, erase content
		timeSelector.addFocusHandler(new FocusHandler() {
			@Override
			public void onFocus(FocusEvent event) { 
				clearErrorMessage();
			}
		});
		
		/* 
		 * do validation on time value
		 * TODO clean this piece of code 
		 *
		 */
		timeSelector.addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				String time = timeSelector.getText();
				if(time.length() < 4 || time.length() > 5){
					timeSelector.addStyleName("input-error");
					timeSelector.setFocus(Boolean.TRUE);
					timeLabel.addStyleName("error"); 
					errorMessagelabel.setText("Erreur saise de l'heure valeur " + time);
					errorMessagelabel.setVisible(true);
					errorMessagelabel.setStyleName("error");
				} else if (time.length() == 4) {
					// validate that each of the char is a digit
					StringBuffer sb = new StringBuffer();
					if (Character.isDigit(time.charAt(0)) && 
						Character.isDigit(time.charAt(1)) && 
						Character.isDigit(time.charAt(2)) && 
						Character.isDigit(time.charAt(3))) {
						
						String hour = String.valueOf(time.charAt(0)) + String.valueOf(time.charAt(1));
						String min = String.valueOf(time.charAt(2)) + String.valueOf(time.charAt(3));
						
						if(validateTime(Integer.valueOf(hour), Integer.valueOf(min))) {
							sb.append(time.charAt(0))
								.append(time.charAt(1))
								.append(":").append(time.charAt(2))
								.append(time.charAt(3));
							
							selectedTime = sb.toString();
							timeSelector.setText(sb.toString());
						}
					} 
				} else if(time.length() == 5 && time.contains(":")){
					String[] validation = time.split(":");
					String hourValidation = validation[0]; 
					String minValidation = validation[1];
					
					validateTime(Integer.valueOf(hourValidation), Integer.valueOf(minValidation));
				}
			}
		});
		
		pilotStatusSelector.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				int selected = pilotStatusSelector.getSelectedIndex();
				selectedPilotStatusId = Long.valueOf(pilotStatusSelector.getValue(selected));
			}
		});
		
		left.setWidget(1, 0, new Label("Statut pilote:"));
		left.setWidget(1, 1, pilotStatusSelector);
		
		sectorSelector.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				int selected = sectorSelector.getSelectedIndex(); 
				selectedSectorId = Long.valueOf(sectorSelector.getValue(selected));
			}
		});
		
		dateSelector  = new DateBox();
		dateSelector.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd MMMM yyyy")));
				
		// default is the current page
		dateSelector.setValue(new Date()); 
		
		dateSelector.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				selectedDate = event.getValue();
			}
		});
		
		right.setWidget(0, 0, new Label("Date:"));
		right.setWidget(0, 1, dateSelector);
		
		right.setWidget(1, 0, new Label("Secteur:"));
		right.setWidget(1, 1, sectorSelector);
		
		// bottom grid
		Grid buttomGrid =  new Grid(1, 2);
		buttomGrid.getCellFormatter().setWidth(0, 1, "256px");
		
		mainPanel.add(mainGrid);
		
		noteEditTextArea = new NoteEditTextArea();
		mainPanel.add(noteEditTextArea);
		
		noteEditTextArea.addFocusHandler(new FocusHandler() {
			@Override
			public void onFocus(FocusEvent event) {
				clearErrorMessage();
			}
		});	
		
		FlowPanel buttonPanel = new FlowPanel();
		buttonPanel.add(saveButton);
		buttonPanel.add(cancelButton);		
		buttonPanel.getElement().setId("dialog-button-panel");
		
		buttomGrid.setWidget(0, 0, errorMessagelabel);
		buttomGrid.setWidget(0, 1, buttonPanel);
		
		mainPanel.add(buttomGrid);
		
		// clean the content of the note edit boxes
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) { 
				noteEditTextArea.setText("");
				hide(); 
			}
		});
		
		mainPanel.add(buttonPanel);
		setWidget(mainPanel);
		
		getElement().setId("edit-note-panel");
	}
	
	private void  clearErrorMessage() {
		if(errorMessagelabel.isVisible())  { 
			errorMessagelabel.setVisible(false);
			errorMessagelabel.setText("");
		} 
		
		timeLabel.removeStyleName("error");
		timeSelector.removeStyleName("input-error");
	}
	
	private boolean validateTime(int hour, int min) {
		boolean valid = false; 
		
		if(hour <= 23 &&  min <= 59) {
			valid = Boolean.TRUE;
		} else {
			timeSelector.addStyleName("input-error");
			timeSelector.setFocus(Boolean.TRUE);
			timeLabel.addStyleName("error");
		}
		
		return valid;
	}
	
	/**
	 * selector index starts at 0
	 * 
	 * @param currentNote
	 */
	public void setCurrentNote(NotePO currentNote) {
		note = currentNote;
		
		note.setNewNote(Boolean.FALSE);
		setNoteTextAreaText("");  
		setNoteTextAreaText(currentNote.getNoteText());
				
		pilotStatusSelector.setSelectedIndex(Integer.valueOf(Integer.valueOf(currentNote.getPilotStatusId().toString())) - 1);
		selectedPilotStatusId = currentNote.getPilotStatusId();

		selectedTime = currentNote.getNoteTime();
		timeSelector.setText(selectedTime);
		
		selectedSectorId = currentNote.getSectorId();
		sectorSelector.setSelectedIndex(Integer.valueOf(Integer.valueOf(currentNote.getSectorId().toString())) - 1);
		
		selectedDate = currentNote.getNoteDate() == null ? new Date() : currentNote.getNoteDate();
		
		dateSelector.setValue(selectedDate);
	}
	
	public NotePO getCurrentNote() {
		// are we in edit more or creation mode?
		if(note == null) note = new NotePO();

		selectedTime = timeSelector.getText();
		
		note.setNoteDate(getSelectedDate())
			.setLastModified(new Date())
			.setNoteTime(timeSelector.getText())
			.setNoteDate(dateSelector.getValue())
			.setNoteText(noteEditTextArea.getText().trim())
			.setNoteStatusId(selectNoteStatusId())
			.setPilotStatusId(selectedPilotStatusId)
			.setSectorId(selectedSectorId);
		
		return note;
	}
	
	/**
	 * validate the time for the automatic note status selection
	 * TODO also validate day
	 *  
	 * @param note
	 * @return
	 */
	private Long selectNoteStatusId() {
		int currentTime =  new Integer(DateTimeFormat.getFormat("Hmm").format(new Date()));
		int setTime = new Integer(timeSelector.getText().replace(":", ""));
		
		boolean isBefore = _clientFactory.isBefore(dateSelector.getValue(), new Date());
		
		Long noteStatusId = noteStatusMap.get(NoteStatus.EN_RETARD).getNoteStatusId();
		
		if(!isBefore || setTime > currentTime) { 
			noteStatusId = noteStatusMap.get(NoteStatus.ATTENTE).getNoteStatusId();
		} 
		
		return noteStatusId;
	}
	
	public void setDialogBoxHeader(String text) {
		this.setText(text);
	}
	
	public void setNoteTextAreaText(String text) {
		noteEditTextArea.setText(text);
	}
	
	public Button getSaveButton() {
		return saveButton;
	}

	public String getSelectedTime() {
		return selectedTime;
	}
	
	public Date getSelectedDate() {
		return selectedDate;
	}

	public Long getSelectedSectorId() {
		return selectedSectorId;
	}
	
	public Long getSelectedPilotStatusId() {
		return selectedPilotStatusId;
	}
	
	public void setSectorValues(List<SectorPO> sectorList) {
		if(sectorValues.isEmpty()) {
			for(SectorPO sector : sectorList) {			
				sectorSelector.addItem(sector.getSectorName(), sector.getSectorId().toString());	
			}
			sectorValues = sectorList; 	
		}
	}
	
	public void setNoteStatusList(List<NoteStatusPO> noteStatusList) {
		for(NoteStatusPO noteStatusPo : noteStatusList) {
			noteStatusMap.put(NoteStatus.find(noteStatusPo.getNoteStatusName()), noteStatusPo);
		}
	}
	
	public void setPilotStatusList(List<PilotStatusPO> pilotStatusList) {
		if(pilotStatusValues.isEmpty()) {
			for(PilotStatusPO pilotStatus : pilotStatusList) {
				pilotStatusSelector.addItem(pilotStatus.getPilotStatusName(), pilotStatus.getPilotStatusId().toString());
			}
			
			pilotStatusValues = pilotStatusList; 	
		}
	}
	
	public void reset(Date newDate) {
		note = new NotePO();
		note.setNewNote(Boolean.TRUE);
		
		noteEditTextArea.setText("");
		
		selectedTime = BasePanel.timeFormat.format(newDate);
		if("0".equals(selectedTime.split(":")[0])) {
			String[] tokens = selectedTime.split(":"); 
			selectedTime = "0" + tokens[0] + ":" + tokens[1];
		}
		
		timeSelector.setText(selectedTime);
		
		if(pilotStatusValues.isEmpty()) {
			setPilotStatusList(_clientFactory.getApplicationController().getPilotStatusList());
		}
		
		selectedPilotStatusId = Long.valueOf(pilotStatusSelector.getValue(0));
		pilotStatusSelector.setSelectedIndex(0);
		
		if(sectorValues.isEmpty()) {
			setSectorValues(_clientFactory.getApplicationController().getSectorList());
		}
		
		selectedSectorId = Long.valueOf(sectorSelector.getValue(0));
		sectorSelector.setSelectedIndex(0);
		selectedDate = newDate;
		dateSelector.setValue(selectedDate);

		setAnimationEnabled(Boolean.TRUE);
		setGlassEnabled(true);
		center();
	}
}