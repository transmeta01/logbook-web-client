package ca.ioniq.web.client.ui.note;

import com.google.gwt.user.client.ui.TextArea;

public class NoteEditTextArea extends TextArea {
	
	public NoteEditTextArea(String text) {
		setCharacterWidth(67);
		setVisibleLines(7);
		setText(text);
	}
	
	public NoteEditTextArea() {
		this("");
	}
}