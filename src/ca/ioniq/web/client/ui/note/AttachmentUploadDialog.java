package ca.ioniq.web.client.ui.note;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.shared.FieldVerifier;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;

public class AttachmentUploadDialog extends DialogBox {
	
	public AttachmentUploadDialog(ClientFactory _clientFactory) {
		final FormPanel uploadForm = new FormPanel();
		uploadForm.setAction(""); // TODO set the uri to call
		uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		uploadForm.setMethod(FormPanel.METHOD_POST);
		
		final DialogBox popup  = new DialogBox();
		uploadForm.setWidget(popup);
		
		final FileUpload uploader = new FileUpload();
		uploader.setName("fileUploade");
		popup.add(uploader);
		
		Button submitButton = new Button("joindre");
		submitButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String fileName = uploader.getFilename();
				// validate
				if(FieldVerifier.isValidName(fileName)) {
					uploadForm.submit();
				} else {
					// show error widget
				}
			}
		});
		
		// add a button and attach click handler
		popup.add(new Button("joindre", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String fileName = uploader.getFilename();
				// validate
				if(FieldVerifier.isValidName(fileName)) {
					uploadForm.submit();
				} else {
					// show error widget
				}
			}
		}));
		
		uploadForm.addSubmitHandler(new FormPanel.SubmitHandler() {
			@Override
			public void onSubmit(SubmitEvent event) {
				// TODO do some validation before the form is submitted
			}
		});
		
		uploadForm.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() { 
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				// read service response
				// the URL of the newly created resource on the server
				
				// TODO construct the Anchor with the file name 
				// TODO add the Anchor to the corresponding note in UI
//				clientFactory.getEventBus().fireEvent(new GetNoteListByDateAndSector(DateTimeFormat.getFormat("dd-MM-yyyy").format(date), sectorId));
				popup.hide(Boolean.TRUE);
				event.getResults();
			}
		});
	}
}
