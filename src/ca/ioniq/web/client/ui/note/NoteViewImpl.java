/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client.ui.note;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestBuilder.Method;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.datepicker.client.DatePicker;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.activity.BaseActivity;
import ca.ioniq.web.client.event.attachment.AttachmentEvent;
import ca.ioniq.web.client.event.attachment.AttachmentEventHandler;
import ca.ioniq.web.client.event.attachment.ViewAttachmentEvent;
import ca.ioniq.web.client.event.note.GetNoteListByDateAndSectorEvent;
import ca.ioniq.web.client.event.note.GetNotesBySectorEvent;
import ca.ioniq.web.client.event.note.NoteEditEvent;
import ca.ioniq.web.client.event.note.NoteEvent;
import ca.ioniq.web.client.event.note.NoteEventHandler;
import ca.ioniq.web.client.event.note.NoteSavedEvent;
import ca.ioniq.web.client.event.note.NoteViewEvent;
import ca.ioniq.web.client.ui.BaseListItemWidget;
import ca.ioniq.web.client.ui.BasePanel;
import ca.ioniq.web.client.ui.BaseUnOrderedList;
import ca.ioniq.web.client.ui.ErrorMessagePanel;
import ca.ioniq.web.client.ui.menu.Navigation;
import ca.ioniq.web.shared.NotePO;
import ca.ioniq.web.shared.NoteStatusPO;
import ca.ioniq.web.shared.PilotStatusPO;
import ca.ioniq.web.shared.SectorPO;

public class NoteViewImpl extends BasePanel implements NoteView {

	private DateDisplayPanel dateDisplayPanel; 
	private NoteEditDialog noteEditDialog;
	private Anchor newNoteAnchor;
	
	private Label messageLabel = new Label();
	private String NOTE_COUNT_MSG = "nombre de note: "; 
	
	private List<NoteDisplayWidget> noteDisplayWidgetList = new ArrayList<NoteDisplayWidget>();
	
	private BaseUnOrderedList notes = new BaseUnOrderedList();
	
	private NoteDisplayWidget currentNoteDisplay; 
	
	private Date selectedDate;
	
	private Long currentSectorId; 
	
	private DatePicker datePicker = new DatePicker(); 
	
	public NoteViewImpl(final ClientFactory clientFactory) {
		super(clientFactory);
		
		FlowPanel mainPanel = new FlowPanel();
		mainPanel.getElement().setId("main-note-panel");
		
		FlowPanel noteHeaderPanel = new FlowPanel();
		noteHeaderPanel.getElement().setId("note-header-panel");
		
		selectedDate = (selectedDate == null) ? today : selectedDate;
		
		dateDisplayPanel = new DateDisplayPanel(_clientFactory, selectedDate);
		
		noteHeaderPanel.add(dateDisplayPanel);
		
		datePicker.setValue(selectedDate, true);
		
		datePicker.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				selectedDate = event.getValue();

				// update date display panel
				dateDisplayPanel.setDateToDisplay(selectedDate);
				
				// get note list for the selected date
				getNoteForSector(selectedDate, currentSectorId);
			}
		});
		
		dateDisplayPanel.addDatePicker(datePicker);
		
		FlowPanel newNoteAnchorPanel = new FlowPanel();
		newNoteAnchor = new Anchor("Nouvelle note");
		newNoteAnchor.setTitle("cr\u00E9ation d'une nouvelle note");
		newNoteAnchor.getElement().setId("new-note-anchor");
		
		newNoteAnchorPanel.getElement().setId("new-note-anchor-panel");
		newNoteAnchorPanel.add(newNoteAnchor);
		
		noteEditDialog = clientFactory.getNoteEditDialog();

		newNoteAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				noteEditDialog.setDialogBoxHeader("Cr\u00E9ation d'une nouvelle note");
				noteEditDialog.reset(new Date());
			}
		});
		
		noteEditDialog.getSaveButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				NotePO note = noteEditDialog.getCurrentNote();
				note.setUsername(clientFactory.getApplicationController().getCurrentUser().getUserName());
				
				// fire event only after validation, else keep the edit note dialog
				if(validate(note, noteEditDialog)) {
					clientFactory.getEventBus().fireEvent(new NoteSavedEvent(note));
					noteEditDialog.hide();
				} else {
					noteEditDialog.reset(selectedDate);
				}
			}
		});

		mainPanel.add(noteHeaderPanel);
		mainPanel.add(newNoteAnchorPanel);
		
		notes.setStyleName("note-list");
		mainPanel.add(notes);
		
		clientFactory.getSectorMenu().getEscQbcAnchor().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) { 
				SectorPO sector = clientFactory.getApplicationController().getSectorByName(Navigation.ESC_QBC.toString());
				currentSectorId = sector.getSectorId();
				getNoteForSector(selectedDate, currentSectorId); 
			}
		});
		
		clientFactory.getSectorMenu().getQbcTrvAnchor().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) { 
				SectorPO sector = clientFactory.getApplicationController().getSectorByName(Navigation.QBC_TRV.toString());
				currentSectorId = sector.getSectorId();
				getNoteForSector(selectedDate, currentSectorId); 
			}
		});
		
		clientFactory.getSectorMenu().getTrvMtlAnchor().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) { 
				SectorPO sector = clientFactory.getApplicationController().getSectorByName(Navigation.TRV_MTL.toString());
				currentSectorId = sector.getSectorId();
				getNoteForSector(selectedDate, currentSectorId);
			}
		});
		
		clientFactory.getSectorMenu().getPortMtlAnchor().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) { 
				SectorPO sector = clientFactory.getApplicationController().getSectorByName(Navigation.PORT_MTL.toString());
				currentSectorId = sector.getSectorId();
				getNoteForSector(selectedDate, currentSectorId); 
			}
		});

		clientFactory.getSectorMenu().getLogbookDispatchAnchor().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				SectorPO sector = clientFactory.getApplicationController()
											   .getSectorByName(Navigation.LOGBOOK_DISPATCH.toString());
				
				currentSectorId = sector.getSectorId();
				getNoteForSector(selectedDate, currentSectorId);
			}
		});
		
		bind();

		initWidget(mainPanel);
	}
	
	private void bind() {
		_clientFactory.getEventBus().addHandler(NoteEvent.TYPE, new NoteEventHandler() {
			String url = URL.encode(BaseActivity.NOTE_BASE_URL);
			
			@Override
			public void onView(NoteViewEvent event) {
				getNoteForSector(selectedDate, currentSectorId);
			}
			
			@Override
			public void onSave(NoteSavedEvent event) {
				final NotePO toSave = event.getNoteToSave();
				
				Method method = RequestBuilder.PUT; 
				if(toSave.getNoteId() != null ) {
					method = RequestBuilder.POST;
				}
				
				RequestBuilder requestBuilder =  new RequestBuilder(method, url);
				requestBuilder.setHeader(BaseActivity.CONTENT_TYPE, BaseActivity.JSON_TYPE);
				
				String json = _clientFactory.getParser().serializeNote(toSave, _clientFactory.getApplicationController().getCurrentUser().getId());
				
				try {
					requestBuilder.sendRequest(json, new POSTNoteViewRequestCallback(event.getNoteToSave()));
				} catch (RequestException e) { /* ignore */ }
			}
			
			@Override
			public void onEdit(NoteEditEvent event) {
				final NotePO note = event.getEditedNote();
				
				RequestBuilder requestBuilder =  new RequestBuilder(RequestBuilder.POST, url);
				requestBuilder.setHeader(BaseActivity.CONTENT_TYPE, BaseActivity.JSON_TYPE);
				
				String json = _clientFactory.getParser().serializeNote(note, _clientFactory.getApplicationController().getCurrentUser().getId());
				
				try {
					requestBuilder.sendRequest(json, new POSTNoteViewRequestCallback(event.getEditedNote()));
				} catch (RequestException e) { /* ignore */ }
			}

			@Override
			public void getNotesBySector(final GetNotesBySectorEvent event) {
				String url = URL.encode(BaseActivity.NOTE_BASE_URL + "/sector/" + event.getSectorId());
				RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);

				try {
					requestBuilder.sendRequest(null, new GETNoteViewRequestCallback());
				} catch (RequestException e) { /* ignore */ }
			}

			@Override
			public void getNoteListByDateAndSector(GetNoteListByDateAndSectorEvent event) {
				String url = URL.encode(BaseActivity.NOTE_BASE_URL + "/" + event.getDate() + "/" + event.getSectorId());
				RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
				
				try {
					requestBuilder.sendRequest(null, new GETNoteViewRequestCallback()); 
				} catch (RequestException e) { /* ignore */ }
			}
		});
		
		// handler attachment event
		_clientFactory.getEventBus().addHandler(AttachmentEvent.TYPE, new AttachmentEventHandler() {
			
			@Override
			public void onViewAttachment(ViewAttachmentEvent event) {
				// get attachment and show attachment
				final String url = URL.encode(BaseActivity.ATTACHMENT_BASE_URL + "/" + event.getAttachmentId());
				RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
				try {
					requestBuilder.sendRequest(null, new RequestCallback() {
						@Override
						public void onResponseReceived(Request request, Response response) {
							int responseCode = response.getStatusCode();
							if (responseCode == Response.SC_OK) {
//								String type = response.getHeader("Content-type");
//								String jsonResponse = response.getText();
//								processResponse(jsonResponse);
								 Window.Location.replace(url); 
							}
						}

						@Override
						public void onError(Request request, Throwable exception) {  /* ignore */  }
					});
					
				} catch(RequestException e) { /* ignore */  }
			}
		});
	}
	
	private boolean validate(NotePO note, NoteEditDialog editDialog) {
		// validate time
		String time = note.getNoteTime();
		// validate test is note text is non empty
		String noteText = note.getNoteText();
		
		return !time.isEmpty() || !(time.length() < 4) ||!(time.length() > 5) && 
				!noteText.isEmpty();
	}
	
	private void processResponse(String jsonResponse) {
		if("[]".equals(jsonResponse)) {
			setNoteList(new ArrayList<NotePO>());
		} else {
			setNoteList(_clientFactory.getParser().deserializeNoteList(jsonResponse));
		}
	}

	private void refreshNoteList(List<NotePO> noteList) {
		notes.clear();
		noteDisplayWidgetList.clear();
		
		notes.add(messageLabel);
		
		if(noteList == null || noteList.isEmpty()) {
			messageLabel.setText(NOTE_COUNT_MSG + 0);
		} else {
			
			// update date display if needed
			dateDisplayPanel.setDateToDisplay(noteList.get(0).getNoteDate());
			
			messageLabel.setText(NOTE_COUNT_MSG + noteList.size());
			
			// re-order notes by time
			sortNoteList(noteList);
			
			for (NotePO note : noteList) {
				NoteDisplayWidget displayWidget = new NoteDisplayWidget(_clientFactory, note);
				BaseListItemWidget noteDiplay = new BaseListItemWidget(displayWidget);

				noteDisplayWidgetList.add(displayWidget);
				notes.add(noteDiplay);
			}
		}
	}

	@Override
	public void setDefaultCurrentSectorId(Long sectorId) {
		currentSectorId = sectorId;
	}
	
	@Override
	public void setNoteList(List<NotePO> noteList) {
		refreshNoteList(noteList);
	}
	
	private void sortNoteList(List<NotePO> noteList) {
		if (noteList.size() > 1 && !noteList.isEmpty()) {
			Collections.sort(noteList, new Comparator<NotePO>() {
				@Override
				public int compare(NotePO note1, NotePO note2) {
					Integer hour1 = new Integer(note1.getNoteTime().replace(":", ""));
					Integer hour2 = new Integer(note2.getNoteTime().replace(":", ""));

					if (hour1 > hour2) return 1;
					if (hour1 < hour2) return -1;
					return 0;
				}
			});
		}
	}

	
	private void getNoteForSector(Date date, Long sectorId) {
		selectedDate = (date == null) ? today : date;
		datePicker.setValue(selectedDate);
		dateDisplayPanel.setDateToDisplay(selectedDate);
		
		SectorPO sector = _clientFactory.getApplicationController().getSectorByName(Navigation.ESC_QBC.toString());
		if(sectorId != null) {
			sector = _clientFactory.getApplicationController().getSectorById(sectorId);
		}
		
		currentSectorId = sector.getSectorId();
		
		_clientFactory.getSectorMenu().setActive(sector.getSectorName());
		
		_clientFactory.getEventBus().fireEvent(new GetNoteListByDateAndSectorEvent(DateTimeFormat.getFormat("dd-MM-yyyy").format(date), currentSectorId));
		
		// switch back to indicate the notes interface is now active
		_clientFactory.getApplicationMenu().setActive(Navigation.NOTES.toString());
	}
	
	@Override
	public void setNoteStatusList(List<NoteStatusPO> noteStatusList) {
		noteEditDialog.setNoteStatusList(noteStatusList);
	}
	
	@Override
	public NotePO getCurrentNote() {
		return currentNoteDisplay.getNote();
	}

	@Override
	public void setPilotStatusList(List<PilotStatusPO> pilotStatusList) {
		noteEditDialog.setPilotStatusList(pilotStatusList);
	}

	@Override
	public void setSectorList(List<SectorPO> sectorList) {
		noteEditDialog.setSectorValues(sectorList);
	}
	
	@Override
	public void setPresenter(Presenter presenter) { }

	@Override
	public void updateNote(NotePO note) {
		currentNoteDisplay.updateWidget(note);
	}
	
	private ErrorMessagePanel errorMessagePanel = new ErrorMessagePanel();
	
	private class GETNoteViewRequestCallback implements RequestCallback {
		@Override
		public void onResponseReceived(Request request, Response response) {
			int responseCode = response.getStatusCode();
			if (responseCode == Response.SC_OK) {
				String jsonResponse = response.getText();
				processResponse(jsonResponse);
			}
		}

		@Override
		public void onError(Request request, Throwable exception) {
			errorMessagePanel.showErrorMessage(exception.getMessage());
		}
	}
	
	private class POSTNoteViewRequestCallback implements RequestCallback {

		private NotePO note; 
		
		public POSTNoteViewRequestCallback(NotePO _notePo) {
			note = _notePo;
		}
		
		@Override
		public void onResponseReceived(Request request, Response response) {
			int responseCode = response.getStatusCode();
			if(responseCode == Response.SC_OK || responseCode == Response.SC_CREATED) {
				getNoteForSector(note.getNoteDate(), note.getSectorId());
			}
		}

		@Override
		public void onError(Request request, Throwable exception) {
			errorMessagePanel.showErrorMessage(exception.getMessage());
		}
	}
}