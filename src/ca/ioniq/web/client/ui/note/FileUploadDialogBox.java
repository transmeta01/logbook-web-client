package ca.ioniq.web.client.ui.note;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.activity.BaseActivity;
import ca.ioniq.web.client.event.note.GetNoteListByDateAndSectorEvent;
import ca.ioniq.web.shared.FieldVerifier;
import ca.ioniq.web.shared.NotePO;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.VerticalPanel;

public class FileUploadDialogBox extends DialogBox {
	
	private String filename;
	private FormPanel uploadForm = new FormPanel();
	private Button attachButton; 
	
	// the note id to which the file is attached and the user id who is executing the action
	public FileUploadDialogBox(final ClientFactory _clientFactory, final NotePO note, Long userId) {
		setText("Joindre un nouveau document");
		setAnimationEnabled(Boolean.TRUE);
		setGlassEnabled(Boolean.TRUE);
		
		// set the uri to call
		uploadForm.setAction(BaseActivity.ATTACHMENT_BASE_URL + "/" + note.getNoteId() + "/" + userId); 
		uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		uploadForm.setMethod(FormPanel.METHOD_POST);
		
		VerticalPanel panel = new VerticalPanel();
		uploadForm.setWidget(panel);

		final FileUpload uploader = new FileUpload();
		uploader.setName("file");
		uploader.setTitle("select a file");
		panel.add(uploader);
		
		FlowPanel buttonPanel = new FlowPanel();
		buttonPanel.setStyleName("dialog-button-panel");
		
		panel.add(buttonPanel);

		// add a button and attach click handler
		attachButton = new Button("joindre", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				filename = uploader.getFilename();
				// validate
				if (FieldVerifier.isValidName(filename)) {
					uploadForm.submit();
				} else {
					// show error widget
				}
			}
		});
		
		buttonPanel.add(attachButton);
		
		buttonPanel.add(new Button("annuler", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				uploadForm.reset();
				hide();
			}
		}));
		
		uploadForm.addSubmitHandler(new FormPanel.SubmitHandler() {
			@Override
			public void onSubmit(SubmitEvent event) {
				// do some validation before the form is submitted
				attachButton.setEnabled(Boolean.TRUE);
			}
		});
		
		uploadForm.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() { 
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				uploadForm.reset();
				_clientFactory.getEventBus().fireEvent(new GetNoteListByDateAndSectorEvent(DateTimeFormat.getFormat("dd-MM-yyyy").format(note.getNoteDate()), note.getSectorId()));
				hide();
			}
		});

		add(uploadForm);
		getElement().setId("file-upload-dialog");
	}
}