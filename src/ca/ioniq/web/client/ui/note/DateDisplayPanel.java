package ca.ioniq.web.client.ui.note;

import java.util.Date;

import ca.ioniq.web.client.ClientFactory;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.datepicker.client.DatePicker;

public class DateDisplayPanel extends Composite {

	private Label dayNumberLabel = new Label();
	private Grid dateGrid = new Grid(1,2);
	
	public DateDisplayPanel(final ClientFactory clientFactory, Date dateToDisplay) {
		dayNumberLabel.getElement().setId("date-display");
		dateGrid.setWidget(0, 0, dayNumberLabel);
		dateGrid.getElement().setId("date-grid");
		
		initWidget(dateGrid);
		setDateToDisplay(dateToDisplay);
	}
	
	public void setDateToDisplay(Date dateToDisplay) {
		StringBuffer date = new StringBuffer();
		date.append(DateTimeFormat.getFormat("d").format(dateToDisplay)).append(" ")
			.append(DateTimeFormat.getFormat("MMMM").format(dateToDisplay)).append(" ")
			.append(DateTimeFormat.getFormat("yyyy").format(dateToDisplay));
		
		dayNumberLabel.setText(date.toString());
	}
	
	public void addDatePicker(DatePicker datePicker) {
		dateGrid.setWidget(0, 1, datePicker);
	}
}