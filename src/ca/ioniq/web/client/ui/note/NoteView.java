package ca.ioniq.web.client.ui.note;

import java.util.List;

import ca.ioniq.web.shared.NotePO;
import ca.ioniq.web.shared.NoteStatusPO;
import ca.ioniq.web.shared.PilotStatusPO;
import ca.ioniq.web.shared.SectorPO;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.IsWidget;

public interface NoteView extends IsWidget {
	
	void setNoteStatusList(List<NoteStatusPO> noteStatusList);
	void setNoteList(List<NotePO> noteList);
	void setPilotStatusList(List<PilotStatusPO> pilotStatusList);
	void setSectorList(List<SectorPO> sectorList);
	void setDefaultCurrentSectorId(Long sectorId);
	
	void updateNote(NotePO note);
	NotePO getCurrentNote();
	
	public interface Presenter {
		void goTo(Place place);
	}

	void setPresenter(Presenter presenter);
}
