package ca.ioniq.web.client.ui.menu;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.FlowPanel;

abstract class BaseMenuPanel extends FlowPanel implements ValueChangeHandler<String> {
	public static String ACTIVE = "active";
	
	public BaseMenuPanel() {}
	
	@Override
	abstract public void onValueChange(ValueChangeEvent<String> event);
}