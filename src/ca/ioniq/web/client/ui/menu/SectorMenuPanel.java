package ca.ioniq.web.client.ui.menu;

import java.util.HashMap;
import java.util.Map;

import ca.ioniq.web.client.ClientFactory;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Anchor;

public class SectorMenuPanel extends BaseMenuPanel {
	
	private Anchor escQbc;
	private Anchor qbcTrv;
	private Anchor trvMtl;
	private Anchor portMtl;
	private Anchor logbookDispatch;
	
	private Anchor active;
	
	private Map<String, Anchor> linkMap = new HashMap<String, Anchor>();
	
	public SectorMenuPanel(final ClientFactory clientFactory) {
		
		escQbc = new Anchor(Navigation.ESC_QBC.toString(), "#" + "note:" + Navigation.ESC_QBC.toString());				
		add(escQbc);
		linkMap.put(Navigation.ESC_QBC.toString(), escQbc);
		escQbc.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				setActive(Navigation.ESC_QBC.toString());
				escQbc.addStyleName(ACTIVE);
			}
		});
		
		qbcTrv = new Anchor(Navigation.QBC_TRV.toString(), "#" + "note:" + Navigation.QBC_TRV.toString());
		add(qbcTrv);
		linkMap.put(Navigation.QBC_TRV.toString(), qbcTrv);
		qbcTrv.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				setActive(Navigation.QBC_TRV.toString());
				qbcTrv.addStyleName(ACTIVE);
			}
		});		
		
		trvMtl = new Anchor(Navigation.TRV_MTL.toString(), "#" + "note:" +Navigation.TRV_MTL.toString());
		add(trvMtl);
		linkMap.put(Navigation.TRV_MTL.toString(), trvMtl);
		trvMtl.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				setActive(Navigation.TRV_MTL.toString());
				trvMtl.addStyleName(ACTIVE);
			}
		});
		
		
		portMtl	= new Anchor(Navigation.PORT_MTL.toString(), "#" + "note:" + Navigation.PORT_MTL.toString());
		add(portMtl);
		linkMap.put(Navigation.PORT_MTL.toString(), portMtl);
		portMtl.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				setActive(Navigation.PORT_MTL.toString());
				portMtl.addStyleName(ACTIVE);
			}
		});
		
		StringBuilder partial = new StringBuilder();
		partial.append("#").append("note:").append(Navigation.LOGBOOK_DISPATCH.toString());
		
		logbookDispatch = new Anchor(Navigation.LOGBOOK_DISPATCH.toString(), partial.toString());
		add(logbookDispatch);
		linkMap.put(Navigation.LOGBOOK_DISPATCH.toString(), logbookDispatch);
		logbookDispatch.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				setActive(Navigation.LOGBOOK_DISPATCH.toString());
				logbookDispatch.addStyleName(ACTIVE);
			}
		});
		
		// default active
		escQbc.addStyleName(ACTIVE);
		active = escQbc;
		
		getElement().setId("sector-menu");
		
		// add this handler to the history mechanism 
		History.addValueChangeHandler(this);
		
		// fire initial history state
		History.fireCurrentHistoryState();
	}

	/**
	 *  This method is called whenever the application's history changes.
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();

		History.newItem(token);
	}
	
	public void setActive(String newActive) {
		active.removeStyleName(ACTIVE);
		
		Anchor activating = linkMap.get(newActive);
		activating.addStyleName(ACTIVE);
		active = activating;
	}
	
	public Anchor getActive() { return active; }

	public Anchor getEscQbcAnchor() {
		return escQbc;		
	}
	
	public Anchor getQbcTrvAnchor() {
		return qbcTrv;
	}

	public Anchor getTrvMtlAnchor() {
		return trvMtl;
	}
	
	public Anchor getPortMtlAnchor() {
		return portMtl;
	}
	
	public Anchor getLogbookDispatchAnchor(){
		return logbookDispatch;
	}
}
