package ca.ioniq.web.client.ui.menu;

public enum Navigation {
	ESC_QBC {
		public String toString() {
			return "ESC-QBC";
		}
	}, 
	
	QBC_TRV {
		public String toString() {
			return "QBC-TRV";
		}
	}, 
	
	TRV_MTL {
		public String toString() {
			return "TRV-MTL";
		}
	}, 
	
	PORT_MTL {
		public String toString() {
			return "PORT-MTL";
		}
	}, 
	
	LOGBOOK_DISPATCH {
		public String toString() { 
			return "LOGBOOK-DISPATCH";
		}
	},
	
	NOTES {
		public String toString() { 
			return "note";
		}
	}, 
	
	AUDIT {
		public String toString() { 
			return "audit";
		}
	}, 
	
	ADMINISTRATION {
		public String toString() { 
			return "admin";
		}
	}, 
	
	USER_ADMIN {
		public String toString() { 
			return "user-admin";
		}
	}, 
	
	PILOT_STATUS_ADMIN {
		public String toString() { 
			return "pilot-status-admin";
		}
	};
	
	public static Navigation find(String name) {
		for(Navigation navigationLink : Navigation.values()) {
			if(navigationLink.toString().equals(name)) {
				return navigationLink; 
			}
		}
		
		return null;
	}
}
