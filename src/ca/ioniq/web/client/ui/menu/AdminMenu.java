package ca.ioniq.web.client.ui.menu;

import ca.ioniq.web.client.ClientFactory;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.ui.Anchor;

public class AdminMenu extends BaseMenuPanel {	
	private Anchor userAdminAnchor;
	private Anchor pilotStatusAdminAnchor;

	public AdminMenu(ClientFactory clientFactory) {
		
		userAdminAnchor = new Anchor(Navigation.USER_ADMIN.toString(), Navigation.USER_ADMIN.toString());
		pilotStatusAdminAnchor = new Anchor(Navigation.ADMINISTRATION.toString(), Navigation.ADMINISTRATION.toString());
		
		userAdminAnchor.addStyleName(ACTIVE);
		
		add(userAdminAnchor);
		add(pilotStatusAdminAnchor);
		
		getElement().setId("admin-menu");
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		// do nothing
	}

}
