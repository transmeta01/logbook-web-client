package ca.ioniq.web.client.ui.menu;

import java.util.HashMap;
import java.util.Map;

import ca.ioniq.web.client.ClientFactory;
import ca.ioniq.web.client.place.AdminPlace;
import ca.ioniq.web.client.place.AuditPlace;
import ca.ioniq.web.client.place.NotePlace;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Anchor;

public class ApplicationMenu extends BaseMenuPanel {
	
	private Anchor notes;
	private Anchor audit;
	private Anchor administration;
	
	private Anchor active;
	
	private final ClientFactory clientFactory;
	
	private Map<String, Anchor> linkMap = new HashMap<String, Anchor>();
	
	public ApplicationMenu(ClientFactory clientFactory) { 
		this.clientFactory = clientFactory;
		
		notes = new Anchor(Navigation.NOTES.toString(), "#" + Navigation.NOTES.toString() + ":view");
		notes.addStyleName(ACTIVE);
		active = notes;
		notes.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				setActive(Navigation.NOTES.toString());
			}
		});
		
		linkMap.put(Navigation.NOTES.toString(), notes);				
		add(notes);
		
		audit = new Anchor(Navigation.AUDIT.toString(), "#" + Navigation.AUDIT.toString() + ":view");
		audit.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				setActive(Navigation.AUDIT.toString());
			}
		});
		
		linkMap.put(Navigation.AUDIT.toString(), audit);		
		add(audit);
		
		administration = new Anchor(Navigation.ADMINISTRATION.toString(), "#" + Navigation.ADMINISTRATION.toString() + ":view");
		administration.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				setActive(Navigation.ADMINISTRATION.toString());
			}
		});
		
		linkMap.put(Navigation.ADMINISTRATION.toString(), administration);		
		add(administration);		
		
		getElement().setId("app-menu");
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();
		
		History.newItem(token);
		
		if(token.contains("notes")) {
			clientFactory.getPlaceController().goTo(new NotePlace("view"));
		} else if(token.contains("audit")) {
			clientFactory.getPlaceController().goTo(new AuditPlace("view"));
		} else {
			clientFactory.getPlaceController().goTo(new AdminPlace("view"));
		}
	}

	public void setActive(String newActive) {
		active.removeStyleName(ACTIVE);
		
		Anchor activating = linkMap.get(newActive);
		activating.addStyleName(ACTIVE);
		active = activating;
	}
	
	public void toggleAdminMenu(boolean toogle) {
		administration.setVisible(toogle);
	}
	
}
