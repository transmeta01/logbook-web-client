/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.client;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.Window.ClosingHandler;

public class LogbookDelegate implements PlaceController.Delegate {
	
	/**
	 *  clean up/release (in the handler) resources before closing
	 */
	@Override
	public HandlerRegistration addWindowClosingHandler(ClosingHandler handler) {
		return null;
	}

	/** 
	 * ask user to confirm application closing
	 */
	@Override
	public boolean confirm(String message) {
		return false;
	}
}
