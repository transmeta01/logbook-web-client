package ca.ioniq.web.client;

import java.util.Date;

import com.google.web.bindery.event.shared.EventBus;
import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.user.client.ui.FlowPanel;

import ca.ioniq.web.client.ui.admin.AdminView;
import ca.ioniq.web.client.ui.audit.AuditView;
import ca.ioniq.web.client.ui.login.LoginView;
import ca.ioniq.web.client.ui.login.LogoutPanel;
import ca.ioniq.web.client.ui.menu.AdminMenu;
import ca.ioniq.web.client.ui.menu.ApplicationMenu;
import ca.ioniq.web.client.ui.menu.SectorMenuPanel;
import ca.ioniq.web.client.ui.note.NoteEditDialog;
import ca.ioniq.web.client.ui.note.NoteView;
import ca.ioniq.web.client.ui.pilotStatus.PilotStatusAdminView;
import ca.ioniq.web.client.ui.user.UserAdminView;
import ca.ioniq.web.shared.parser.CustomJSONParser;

public interface ClientFactory {
	
	EventBus getEventBus();
	PlaceController getPlaceController();
	LogbookController getApplicationController();
	ActivityManager getActivityManager();
	ActivityMapper getActivityMapper();
	PlaceHistoryMapper getPlaceHistoryMapper();
	PlaceHistoryHandler getPlaceHistoryHandler();
	
	//views
	LoginView getLoginView();
	FlowPanel getMainContentPanel();
	FlowPanel getHeaderPanel();
	
	AdminMenu getAdminMenu();
	SectorMenuPanel getSectorMenu();
	ApplicationMenu getApplicationMenu();
	FlowPanel getMainMenu();
	
	LogoutPanel getLogoutPanel();
	
	// note view widget
	NoteView getNoteView();
	NoteEditDialog getNoteEditDialog();
	
	// admin view widgets
	AdminView getAdminView();
	UserAdminView getUserAdminView();
	PilotStatusAdminView getPilotStatusAdminView();
	
	AuditView getAuditView();
	
	CustomJSONParser getParser();
	
	public boolean isSameDay(Date date1, Date date2);
	public boolean isBefore(Date date1, Date date2);
	public boolean isBefore(String dateString1, String dateString2);
	
}
