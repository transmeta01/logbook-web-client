package ca.ioniq.web.client.event.admin;

import ca.ioniq.web.client.event.pilotStatus.AdminPilotStatusEditEvent;
import ca.ioniq.web.client.event.pilotStatus.AdminPilotStatusListViewEvent;
import ca.ioniq.web.client.event.user.AdminUserEditEvent;
import ca.ioniq.web.client.event.user.AdminUserListViewEvent;

import com.google.gwt.event.shared.EventHandler;

public interface AdminEventHandler extends EventHandler {
	void onViewUserList(AdminUserListViewEvent event);
	void onUserEditSave(AdminUserEditEvent event);
	
	void onPilotStatusListView(AdminPilotStatusListViewEvent event);
	void onPiloteStatusEditSave(AdminPilotStatusEditEvent event);
}
