package ca.ioniq.web.client.event.note;

import ca.ioniq.web.shared.NotePO;

public class NoteSavedEvent extends NoteEvent {

	private NotePO noteToSave;
	
	public NoteSavedEvent(NotePO note) {
		this.noteToSave = note;
	}

	@Override
	protected void dispatch(NoteEventHandler handler) {
		handler.onSave(this);
	}

	public NotePO getNoteToSave() {
		return noteToSave;
	}
}
