package ca.ioniq.web.client.event.note;

import ca.ioniq.web.shared.NotePO;

public class NoteEditEvent extends NoteEvent {

	private NotePO editedNote;
	public NoteEditEvent(NotePO editedNote) {
		this.editedNote = editedNote;
	}
	
	@Override
	protected void dispatch(NoteEventHandler handler) {
		handler.onEdit(this);
	}

	public NotePO getEditedNote() {
		return editedNote;
	}
}