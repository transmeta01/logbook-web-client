package ca.ioniq.web.client.event.note;

import com.google.gwt.event.shared.GwtEvent;

public abstract class NoteEvent extends GwtEvent<NoteEventHandler> {

	public static Type<NoteEventHandler> TYPE = new Type<NoteEventHandler>();
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<NoteEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	abstract protected void dispatch(NoteEventHandler handler);

}
