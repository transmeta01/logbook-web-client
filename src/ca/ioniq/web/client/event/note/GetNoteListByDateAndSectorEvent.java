package ca.ioniq.web.client.event.note;

public class GetNoteListByDateAndSectorEvent extends NoteEvent {
	
	private String _date; // format: dd-MM-yyyy
	private Long _sectorId;

	public GetNoteListByDateAndSectorEvent(String date, Long sectorId) {
		_date = date;
		_sectorId = sectorId;
	}
	
	@Override
	protected void dispatch(NoteEventHandler handler) {
		handler.getNoteListByDateAndSector(this);
	}

	public String getDate() {
		return _date;
	}

	public Long getSectorId() {
		return _sectorId;
	}
	
}
