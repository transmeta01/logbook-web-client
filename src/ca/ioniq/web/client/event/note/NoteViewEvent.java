package ca.ioniq.web.client.event.note;

public class NoteViewEvent extends NoteEvent {

	@Override
	protected void dispatch(NoteEventHandler handler) {
		handler.onView(this);
	}

}
