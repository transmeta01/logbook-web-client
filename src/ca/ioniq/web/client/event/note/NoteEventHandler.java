package ca.ioniq.web.client.event.note;

import com.google.gwt.event.shared.EventHandler;

public interface NoteEventHandler extends EventHandler {
	void onView(NoteViewEvent event);
	void onEdit(NoteEditEvent event);
	void onSave(NoteSavedEvent event);
	
	void getNotesBySector(GetNotesBySectorEvent event);
	void getNoteListByDateAndSector(GetNoteListByDateAndSectorEvent event);
}
