package ca.ioniq.web.client.event.note;

public class GetNotesBySectorEvent extends NoteEvent {
	
	private Long _sectorId; 
	
	public GetNotesBySectorEvent(Long sectorId) {
		_sectorId = sectorId;
	}

	@Override
	protected void dispatch(NoteEventHandler handler) {
		handler.getNotesBySector(this);
	}

	public Long getSectorId() {
		return _sectorId;
	}
}
