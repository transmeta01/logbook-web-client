package ca.ioniq.web.client.event.attachment;

import com.google.gwt.event.shared.EventHandler;

public interface AttachmentEventHandler extends EventHandler {
	void onViewAttachment(ViewAttachmentEvent event);
}