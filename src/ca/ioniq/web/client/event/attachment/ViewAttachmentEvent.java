package ca.ioniq.web.client.event.attachment;

public class ViewAttachmentEvent extends AttachmentEvent {

	public static com.google.gwt.event.shared.GwtEvent.Type<AttachmentEventHandler> TYPE = new Type<AttachmentEventHandler>();
	
	private long attachmentId; 
	
	public ViewAttachmentEvent(long attachmentId) {
		this.attachmentId = attachmentId;
	}
	
	@Override
	protected void dispatch(AttachmentEventHandler handler) {
		handler.onViewAttachment(this);
	}

	public long getAttachmentId() {
		return attachmentId;
	}
}
