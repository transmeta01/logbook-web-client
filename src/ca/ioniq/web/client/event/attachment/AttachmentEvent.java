package ca.ioniq.web.client.event.attachment;

import com.google.gwt.event.shared.GwtEvent;

public abstract class AttachmentEvent extends GwtEvent<AttachmentEventHandler> {

	public static Type<AttachmentEventHandler> TYPE = new Type<AttachmentEventHandler>();
	
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AttachmentEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	abstract protected void dispatch(AttachmentEventHandler handler);
}
