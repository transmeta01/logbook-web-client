package ca.ioniq.web.client.event.audit;

import com.google.gwt.event.shared.GwtEvent;

public abstract class AuditEvent extends GwtEvent<AuditEventHandler> {
	
	public static Type<AuditEventHandler> TYPE = new Type<AuditEventHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AuditEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	abstract protected void dispatch(AuditEventHandler handler);

}