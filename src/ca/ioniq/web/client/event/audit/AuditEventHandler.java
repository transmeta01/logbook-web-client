package ca.ioniq.web.client.event.audit;

import com.google.gwt.event.shared.EventHandler;

public interface AuditEventHandler extends EventHandler {
	void onViewAuditBySector(ViewAuditBySectorEvent event);
	void onFilterAudit(FilterAuditEvent event);
}