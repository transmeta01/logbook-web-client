package ca.ioniq.web.client.event.audit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.i18n.client.DateTimeFormat;

import ca.ioniq.web.shared.AuditPO;

public class AuditFilter {
	
	private List<AuditPO> dateList = new ArrayList<AuditPO>();
	private List<AuditPO> roleList = new ArrayList<AuditPO>();
	private List<AuditPO> keywordsList = new ArrayList<AuditPO>();
	
	public AuditFilter() {
	}
	
	public List<AuditPO> filter(List<AuditPO> filterList, Date startDate, Date endDate, String role, String keywords) {
		return filterByKeyWords(filterByRole(filterByDate(filterList, startDate, endDate), role), keywords);
	}

	public List<AuditPO> filterByRole(List<AuditPO> list, String role) {
		roleList.clear();
		
		if(role.equals("")) return list;

		for (AuditPO audit : list) {
			if (audit.getRole().equals(role)) {
				roleList.add(audit);
			}
		}

		return roleList;
	}

	public List<AuditPO> filterByDate(List<AuditPO> list, Date startDate, Date endDate) {
		dateList.clear();
		
		// need to modify date so the selection is inclusive
		startDate = new Date(startDate.getTime() - (1000 * 60 * 60 * 24));
		endDate = new Date(endDate.getTime() + (1000 * 60 * 60 * 24));

		Date auditDate;
		for (AuditPO audit : list) {
			auditDate = DateTimeFormat.getFormat("dd-MM-yyyy").parse(audit.getDate());
			if (auditDate.after(startDate) && auditDate.before(endDate)) {
				dateList.add(audit);
			}
		}

		Collections.sort(dateList, new Comparator<AuditPO>() {
			@Override
			public int compare(AuditPO audit1, AuditPO audit2) {
				return audit1.getDate().compareTo(audit2.getDate());
			}
		});

		return dateList;
	}

	/**
	 * @FIXME this method is quadratic....yikes!!!!
	 * 
	 * @param list
	 * @param keywords
	 * @return
	 */
	public List<AuditPO> filterByKeyWords(List<AuditPO> list, String keywords) {
		keywordsList.clear();

		if(keywords.equals("")) return list;
		
		List<String> keywordList = Arrays.asList(keywords.split(" "));
		for (String word : keywordList) {
			for (AuditPO audit : list) {
				if(audit.getNoteText().contains(word)) {
					keywordsList.add(audit);
				}
			}
		}

		return keywordsList;
	}
}