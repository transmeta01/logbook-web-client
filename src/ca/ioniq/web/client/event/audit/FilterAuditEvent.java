package ca.ioniq.web.client.event.audit;

import java.util.Date;

public class FilterAuditEvent extends AuditEvent {
	
	private Date startDate;
	private Date endDate;
	private String role;
	private String keywords;
	
	public FilterAuditEvent(Date _startDate, Date _endDate, String _role, String _keywords) {
		startDate = _startDate;
		endDate = _endDate;
		role = _role;
		keywords = _keywords;
	}

	@Override
	protected void dispatch(AuditEventHandler handler) {
		handler.onFilterAudit(this);
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public String getRole() {
		return role;
	}

	public String getKeywords() {
		return keywords;
	}
}
