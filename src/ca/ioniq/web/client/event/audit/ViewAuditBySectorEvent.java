package ca.ioniq.web.client.event.audit;

public class ViewAuditBySectorEvent extends AuditEvent {
	
	private String sectorname;
	
	public ViewAuditBySectorEvent(String _sectorname) {
		sectorname = _sectorname;
	}

	@Override
	protected void dispatch(AuditEventHandler handler) {
		handler.onViewAuditBySector(this);
	}

	public String getSectorname() {
		return sectorname;
	}
}
