package ca.ioniq.web.client.event.pilotStatus;

import ca.ioniq.web.client.event.admin.AdminEventHandler;
import ca.ioniq.web.shared.PilotStatusPO;

import com.google.gwt.event.shared.GwtEvent;

public class AdminPilotStatusEditEvent extends GwtEvent<AdminEventHandler> {

	public static Type<AdminEventHandler> TYPE = new Type<AdminEventHandler>();
	public final PilotStatusPO editPilotStatus;
	
	public AdminPilotStatusEditEvent(PilotStatusPO editPilotStatus) {
		this.editPilotStatus = editPilotStatus;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AdminEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AdminEventHandler handler) {
		handler.onPiloteStatusEditSave(this);
	}

	public PilotStatusPO getEditPilotStatus() {
		return editPilotStatus;
	}
}
