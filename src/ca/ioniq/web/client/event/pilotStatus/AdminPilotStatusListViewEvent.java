package ca.ioniq.web.client.event.pilotStatus;

import com.google.gwt.event.shared.GwtEvent;

import ca.ioniq.web.client.event.admin.AdminEventHandler;

public class AdminPilotStatusListViewEvent extends GwtEvent<AdminEventHandler> {
	public static Type<AdminEventHandler> TYPE = new Type<AdminEventHandler>();
	
	public AdminPilotStatusListViewEvent() {}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AdminEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AdminEventHandler handler) {
		handler.onPilotStatusListView(this);
	}
}