package ca.ioniq.web.client.event.user;

import com.google.gwt.event.shared.GwtEvent;

import ca.ioniq.web.client.event.admin.AdminEventHandler;
import ca.ioniq.web.shared.UserPO;

public class AdminUserEditEvent extends GwtEvent<AdminEventHandler> {
	
	private UserPO editUser;
	
	public AdminUserEditEvent(UserPO user) {
		this.editUser = user;
	}

	public static Type<AdminEventHandler> TYPE = new Type<AdminEventHandler>();
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AdminEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AdminEventHandler handler) {
		handler.onUserEditSave(this);
	}

	public UserPO getEditUser() {
		return editUser;
	}
	
}
