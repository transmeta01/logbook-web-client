package ca.ioniq.web.client.event.user;

import ca.ioniq.web.client.event.admin.AdminEventHandler;

import com.google.gwt.event.shared.GwtEvent;

public class AdminUserListViewEvent extends GwtEvent<AdminEventHandler> {

	public static Type<AdminEventHandler> TYPE = new Type<AdminEventHandler>();
	
	public AdminUserListViewEvent() { }
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AdminEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AdminEventHandler handler) {
		handler.onViewUserList(this);
	}
}