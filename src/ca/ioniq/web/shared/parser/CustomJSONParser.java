/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.shared.parser;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;

import ca.ioniq.web.shared.AttachmentPO;
import ca.ioniq.web.shared.AuditPO;
import ca.ioniq.web.shared.NotePO;
import ca.ioniq.web.shared.NoteStatusPO;
import ca.ioniq.web.shared.PilotStatusPO;
import ca.ioniq.web.shared.SectorPO;
import ca.ioniq.web.shared.UserPO;

public class CustomJSONParser {
	

	public CustomJSONParser() {}

	public List<PilotStatusPO> deserializePilotStatusList(String json) {
		List<PilotStatusPO> pilotStatusList = new ArrayList<PilotStatusPO>();

		JSONArray pilotStatusArray = JSONParser.parseLenient(json).isArray();

		for (int i = 0; i < pilotStatusArray.size(); i++) {
			JSONObject pilotObject = pilotStatusArray.get(i).isObject();
			PilotStatusPO pilotStatus = new PilotStatusPO();

			pilotStatus.setPilotStatusId(new Double(pilotObject.get("id").isNumber().doubleValue()).longValue())
					   .setPilotStatusDescription(pilotObject.get("description").isString().stringValue())
					   .setPilotStatusCode(pilotObject.get("statusCode").isString().stringValue())
					   .setPilotStatusName(pilotObject.get("status").isString().stringValue())
					   .setActive(pilotObject.get("active").isBoolean().booleanValue());

			pilotStatusList.add(pilotStatus);
		}

		return pilotStatusList;
	}
	
	public String serializePilotStatusPO(PilotStatusPO pilotStatus, Long userid) {
		JSONObject pilotStatusObj = new JSONObject();

        if(pilotStatus.getPilotStatusId() == null) {
                pilotStatusObj.put("id", new JSONString(""));
        } else {
                pilotStatusObj.put("id", new JSONNumber(pilotStatus.getPilotStatusId()));
        }
		
		pilotStatusObj.put("description", new JSONString(pilotStatus.getPilotStatusDescription()));
		pilotStatusObj.put("statusCode", new JSONString(pilotStatus.getPilotStatusDescription()));
		pilotStatusObj.put("status", new JSONString(pilotStatus.getPilotStatusDescription()));
		pilotStatusObj.put("active", JSONBoolean.getInstance(pilotStatus.isActive()));
		
		return pilotStatusObj.toString();
	}

	public List<SectorPO> deserializeSector(String json) {
		 List<SectorPO> sectorList = new ArrayList<SectorPO>();

		JSONArray sectorArray = JSONParser.parseLenient(json).isArray();
		
		for(int i = 0; i < sectorArray.size(); i++) {
			SectorPO sector = new SectorPO();
			JSONObject sectorObject = sectorArray.get(i).isObject();
			
			sector.setSectorId(new Double(sectorObject.get("id").isNumber().doubleValue()).longValue());
			sector.setSectorName(sectorObject.get("sectorname").isString().stringValue());
			
			sectorList.add(sector);
		}
		
		return sectorList;
	}

	public List<UserPO> deserializeUserList(String json) {
		List<UserPO> userList = new ArrayList<UserPO>();
		JSONArray userArray = JSONParser.parseLenient(json).isArray();
		
		for(int i = 0; i < userArray.size(); i++) {
			JSONObject userObj = userArray.get(i).isObject();
			
			UserPO user = deserializeSingleUser(userObj);
			
			userList.add(user);
		}

		return userList;
	}
	
	public UserPO deserializeSingleUser(JSONObject userObj) {
		UserPO user = new UserPO();
		user.setId(new Double(userObj.get("id").isNumber().doubleValue()).longValue())
			.setUserName(userObj.get("username").isString().stringValue())
			.setLastName(userObj.get("lastname").isString().stringValue())
			.setFirstName(userObj.get("firstname").isString().stringValue())
			.setRole(userObj.get("role").isString().stringValue())
			.setActive(userObj.get("active").isBoolean().booleanValue());

		return user;
	}

	public UserPO deserializeSingleUser(String json) {
		JSONObject value = JSONParser.parseLenient(json).isObject();
		UserPO user = new UserPO();
		user.setId(new Double(value.get("id").isNumber().doubleValue()).longValue())
			.setUserName(value.get("username").isString().stringValue())
			.setLastName(value.get("lastname").isString().stringValue())
			.setFirstName(value.get("firstname").isString().stringValue())
			.setRole(value.get("role").isString().stringValue())
			.setActive(value.get("active").isBoolean().booleanValue());;

		return user;
	}
	
	public String serializUser(UserPO user) {
		JSONObject userObj = new JSONObject();
		
		if(user.getId() != null) {
			userObj.put("id", new JSONNumber(user.getId()));
		} else {
			userObj.put("id", new JSONString(""));
		}
		
		userObj.put("username", new JSONString(user.getUserName()));
		userObj.put("lastname", new JSONString(user.getLastName()));
		userObj.put("firstname", new JSONString(user.getFirstName()));
		userObj.put("role", new JSONString(user.getRole()));
		userObj.put("active", JSONBoolean.getInstance(user.isActive()));
		
		return userObj.toString();
	}

	public String serializeNote(NotePO note, Long userId) {
		JSONObject noteObj = new JSONObject();
		
		if(note.getNoteId() != null) {
			noteObj.put("id", new JSONNumber(note.getNoteId()));
		} else {
			noteObj.put("id", new JSONString(""));
		}
		
		noteObj.put("sector_id", new JSONNumber(note.getSectorId()));
		noteObj.put("noteText", new JSONString(note.getNoteText()));
		noteObj.put("pilotStatus_id", new JSONNumber(note.getPilotStatusId()));
		noteObj.put("noteStatus_id", new JSONNumber(note.getNoteStatusId()));
		noteObj.put("date", new JSONString(DateTimeFormat.getFormat("dd MM yyyy").format(note.getNoteDate())));
		noteObj.put("time", new JSONString(note.getNoteTime()));
		noteObj.put("username", new JSONString(note.getUsername()));
		noteObj.put("user_id", new JSONNumber(userId));
		
		return noteObj.toString();
	}

	public List<NotePO> deserializeNoteList(String json) {
		json  = json.replace("\n", "\\n").replace("\r", "\\r");
		List<NotePO> NotePOsList = new ArrayList<NotePO>();

		JSONArray noteArray = JSONParser.parseLenient(json).isArray();

		for (int i = 0; i < noteArray.size(); i++) {
			JSONObject noteObject = noteArray.get(i).isObject();

			NotePO note = new NotePO();
			
			String date = noteObject.get("noteDate").isString().stringValue().split(" ")[0];
			note.setNoteDate(DateTimeFormat.getFormat("yyyy-MM-dd").parse(date))
					.setNoteTime(noteObject.get("noteTime").isString().stringValue())
					.setUsername(noteObject.get("username").isString().stringValue())
					.setNoteId(new Double(noteObject.get("id").isNumber().doubleValue()).longValue());
			
			String noteText = new String(noteObject.get("noteText").isString().stringValue().trim().replace("\\n", "\n").replace("\\r", "\r"));
			note.setNoteText(noteText);

			Long sectorId = new Double(noteObject.get("sector_id").isNumber().doubleValue()).longValue();
			note.setSectorId(sectorId);

			Long noteStatusId = new Double(noteObject.get("noteStatus_id").isNumber().doubleValue()).longValue();
			note.setNoteStatusId(noteStatusId);

			Long pilotStatusId = new Double(noteObject.get("pilotStatus_id").isNumber().doubleValue()).longValue();
			note.setPilotStatusId(pilotStatusId);
			
			note.setNoteTime(noteObject.get("noteTime").isString().stringValue());

			note.setLastModified(DateTimeFormat.getFormat("yyyy-MM-dd hh:mm:ss.S").parse(noteObject.get("lastModified").isString().stringValue()));

			note.setNewNote(Boolean.FALSE);
			
			// process note attachments
			List<AttachmentPO> attachements = note.getAttachmentList(); 
			if(attachements == null) { 
				attachements = new ArrayList<AttachmentPO>();
				note.setAttachmentList(attachements);
			}

			JSONArray attatchements = noteObject.get("attachments").isArray();
			for (int attachmentIndex = 0; attachmentIndex < attatchements.size(); attachmentIndex++) {
				JSONObject attachObj = attatchements.get(attachmentIndex).isObject();
				AttachmentPO noteAttachement = new AttachmentPO();
				noteAttachement.setId(new Double(attachObj.get("id").isNumber().doubleValue()).longValue()) 
							   .setDocumentName(attachObj.get("name").isString().stringValue());
				
				attachements.add(noteAttachement);
			}

			NotePOsList.add(note);
		}

		return NotePOsList;
	}

	public List<NoteStatusPO> deserializeNoteStatusList(String json) {
		JSONArray noteStatusArray = JSONParser.parseLenient(json).isArray();
		
		List<NoteStatusPO> noteStatusList = new ArrayList<NoteStatusPO>();
		
		for(int i = 0; i < noteStatusArray.size(); i++) {
			NoteStatusPO noteStatus = new NoteStatusPO();
			JSONObject noteStatusOb = noteStatusArray.get(i).isObject();
			noteStatus.setNoteStatusId(new Double(noteStatusOb.get("id").isNumber().doubleValue()).longValue())
					  .setNoteStatusName(noteStatusOb.get("status").isString().stringValue());
			
			noteStatusList.add(noteStatus);
		}
		
		return noteStatusList;
	}

	public List<AuditPO> deserializeAuditList(String json) {
		json  = json.replace("\n", "\\n").replace("\r", "\\r");
		JSONArray auditArray = JSONParser.parseLenient(json).isArray();
		List<AuditPO> auditList = new ArrayList<AuditPO>();
		
		for(int i = 0; i < auditArray.size(); i++) {
			AuditPO audit = new AuditPO();
			JSONObject auditObj = auditArray.get(i).isObject();
			audit.setId(new Double(auditObj.get("id").isNumber().doubleValue()).longValue())
				 .setTime(auditObj.get("time").isString().stringValue())
				 .setStatus(auditObj.get("note_status").isString().stringValue())
				 .setPilotStatus(auditObj.get("pilot_status").isString().stringValue())
				 .setUsername(auditObj.get("username").isString().stringValue())
				 .setRole(auditObj.get("role").isString().stringValue())
				 .setNoteText(auditObj.get("noteText").isString().stringValue())
				 .setSector(auditObj.get("sector").isString().stringValue())
				 .setDate(auditObj.get("date").isString().stringValue())
				 .setAttachedDocument(auditObj.get("attachment").isString().stringValue())
				 .setNoteId(new Double(auditObj.get("note_id").isNumber().doubleValue()).longValue());
			
			auditList.add(audit);
		}
		
		return auditList;
	}
}
