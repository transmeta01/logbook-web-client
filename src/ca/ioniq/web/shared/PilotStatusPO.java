/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.shared;

public class PilotStatusPO {
	private Long pilotStatusId;
	private String pilotStatusName;
	private String pilotStatusDescription;
	private String pilotStatusCode;
	private boolean active;
	
	public PilotStatusPO() {}

	public PilotStatusPO(Long pilotStatusId, String pilotStatusName, String pilotStatusDescription, String pilotStatusCode, boolean active) {
		this.pilotStatusId = pilotStatusId;
		this.pilotStatusName = pilotStatusName;
		this.pilotStatusDescription = pilotStatusDescription;
		this.pilotStatusCode = pilotStatusCode;
		this.active = active;
	}

	public Long getPilotStatusId() {
		return pilotStatusId;
	}

	public PilotStatusPO setPilotStatusId(Long pilotStatusId) {
		this.pilotStatusId = pilotStatusId;
		return this;
	}

	public String getPilotStatusName() {
		return pilotStatusName;
	}

	public PilotStatusPO setPilotStatusName(String pilotStatusName) {
		this.pilotStatusName = pilotStatusName;
		return this;
	}

	public String getPilotStatusDescription() {
		return pilotStatusDescription;
	}

	public PilotStatusPO setPilotStatusDescription(String pilotStatusDescription) {
		this.pilotStatusDescription = pilotStatusDescription;
		return this;
	}

	public String getPilotStatusCode() {
		return pilotStatusCode;
	}

	public PilotStatusPO setPilotStatusCode(String pilotStatusCode) {
		this.pilotStatusCode = pilotStatusCode;
		return this;
	}

	public boolean isActive() {
		return active;
	}

	public PilotStatusPO setActive(boolean active) {
		this.active = active;
		return this;
	}
}
