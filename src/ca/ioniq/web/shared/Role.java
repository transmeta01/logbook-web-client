/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.shared;

public enum Role {
	NO_ROLE {
		public String toString() {
			return "";
		}
	}, 
	
	DISPATCHER {
		public String toString() {
			return "Répartiteur";
		}
	}, 
	
	ADMIN {
		public String toString() {
			return "Administrateur";
		}
	}, 
	
	DIRECTOR {
		public String toString() {
			return "Directrice affectation";
		}
	},

	SUPERVISOR_DISPATCH {
		public String toString() { 
			return "Surveillant répartiteur";
		}
	}; 
	
	public static Role find(String name) {
		for(Role role : Role.values()) {
			if(role.toString().equals(name)) {
				return role;
			}
		}
		
		return null;
	}
}
