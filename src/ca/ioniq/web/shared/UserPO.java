/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.shared;

public class UserPO {
	private Long id;
	private String userName;
	private String firstName;
	private String lastName;
	private String role;
	private boolean active;
	
	public UserPO() {}

	public UserPO(Long id, String userName, String firstName, String lastName, String role, boolean active) {
		this.id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.role = role;
		this.active = active;
	}
	
	public Long getId() {
		return id;
	}

	public UserPO setId(Long id) {
		this.id = id;
		return this;
	}

	public String getUserName() {
		return userName;
	}

	public UserPO setUserName(String userName) {
		this.userName = userName;
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public UserPO setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public UserPO setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public String getRole() {
		return role;
	}

	public UserPO setRole(String role) {
		this.role = role;
		return this;
	}

	public boolean isActive() {
		return active;
	}

	public UserPO setActive(boolean active) {
		this.active = active;
		return this;
	}
}
