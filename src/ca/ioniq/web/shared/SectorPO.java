/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.shared;

public class SectorPO {
	private Long sectorId;
	private String sectorname;
	
	public SectorPO() {}

	public SectorPO(Long sectorId, String sectorName) {
		this.sectorId = sectorId;
		this.sectorname = sectorName;
	}

	public Long getSectorId() {
		return sectorId;
	}

	public void setSectorId(Long sectorId) {
		this.sectorId = sectorId;
	}

	public String getSectorName() {
		return sectorname;
	}

	public void setSectorName(String sectorName) {
		this.sectorname = sectorName;
	}
}
