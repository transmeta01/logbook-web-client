/**
 * Copyright 2012 Logbook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.web.shared;

import java.util.Date;
import java.util.List;

public class NotePO {
	private Long noteId;
	private Long sectorId;
	private String noteText;
	private Long noteStatusId;
	private Long pilotStatusId;
	private Date noteDate;
	private String noteTime;
	private String username;
	
	private Boolean newNote = Boolean.FALSE;
	
	private Date lastModified;
	
	private List<AttachmentPO> attachmentList;
	
	public NotePO() {}

	public Long getNoteId() {
		return noteId;
	}

	public NotePO setNoteId(Long noteId) {
		this.noteId = noteId;
		return this;
	}

	public Long getSectorId() {
		return sectorId;
	}

	public NotePO setSectorId(Long sectorId) {
		this.sectorId = sectorId;
		return this;
	}

	public String getNoteText() {
		return noteText;
	}

	public NotePO setNoteText(String noteText) {
		this.noteText = noteText;
		return this;
	}

	public Long getNoteStatusId() {
		return noteStatusId;
	}

	public NotePO setNoteStatusId(Long noteStatusId) {
		this.noteStatusId = noteStatusId;
		return this;
	}

	public Long getPilotStatusId() {
		return pilotStatusId;
	}

	public NotePO setPilotStatusId(Long pilotStatusId) {
		this.pilotStatusId = pilotStatusId;
		return this;
	}

	public Date getNoteDate() {
		return noteDate;
	}

	public NotePO setNoteDate(Date noteDate) {
		this.noteDate = noteDate;
		return this;
	}

	public String getNoteTime() {
		return noteTime;
	}

	public NotePO setNoteTime(String noteTime) {
		this.noteTime = noteTime;
		return this;
	}

	public String getUsername() {
		return username;
	}

	public NotePO setUsername(String username) {
		this.username = username;
		return this;
	}

	public Boolean isNewNote() {
		return newNote;
	}

	public NotePO setNewNote(Boolean newNote) {
		this.newNote = newNote;
		return this;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public NotePO setLastModified(Date lastModified) {
		this.lastModified = lastModified;
		return this;
	}

	public List<AttachmentPO> getAttachmentList() {
		return attachmentList;
	}

	public NotePO setAttachmentList(List<AttachmentPO> attachmentList) {
		this.attachmentList = attachmentList;
		return this;
	}
}