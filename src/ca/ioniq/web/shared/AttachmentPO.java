package ca.ioniq.web.shared;

public class AttachmentPO {
	private Long id;
	private String documentName;
	
	public AttachmentPO() { }

	public String getDocumentName() {
		return documentName;
	}

	public AttachmentPO setDocumentName(String documentName) {
		this.documentName = documentName;
		return this;
	}

	public Long getId() {
		return id;
	}
	
	public AttachmentPO setId(Long id) {
		this.id = id;
		return this;
	}
}
