package ca.ioniq.web.shared;

public class AuditPO {
	private Long id;
	private String time;
	private String date;
	private String status;
	private String pilotStatus;
	private Long noteId;
	private String username;
	private String role;
	private String noteText;
	private String sector;
	private String attachedDocuments;
	
	public AuditPO() {}
	
	public Long getId() {
		return id;
	}
	
	public AuditPO setId(Long id) {
		this.id = id;
		return this;
	}

	public String getTime() {
		return time;
	}

	public AuditPO setTime(String time) {
		this.time = time;
		return this;
	}

	public String getDate() {
		return date;
	}

	public AuditPO setDate(String date) {
		this.date = date;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public AuditPO setStatus(String status) {
		this.status = status;
		return this;
	}

	public Long getNoteId() {
		return noteId;
	}

	public AuditPO setNoteId(Long noteId) {
		this.noteId = noteId;
		return this;
	}

	public String getUsername() {
		return username;
	}

	public AuditPO setUsername(String username) {
		this.username = username;
		return this;
	}

	public String getNoteText() {
		return noteText;
	}

	public AuditPO setNoteText(String noteText) {
		this.noteText = noteText;
		return this;
	}

	public String getSector() {
		return sector;
	}

	public AuditPO setSector(String sector) {
		this.sector = sector;
		return this;
	}

	public String getAttachedDocument() {
		return attachedDocuments;
	}

	public AuditPO setAttachedDocument(String _attachedDocuments) {
		attachedDocuments = _attachedDocuments;
		return this;
	}

	public String getPilotStatus() {
		return pilotStatus;
	}

	public AuditPO setPilotStatus(String pilotStatus) {
		this.pilotStatus = pilotStatus;
		return this;
	}

	public String getRole() {
		return role;
	}

	public AuditPO setRole(String role) {
		this.role = role;
		return this;
	}
}
