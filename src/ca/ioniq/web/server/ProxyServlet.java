package ca.ioniq.web.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.Header;

/**
 * This servlet is strictly for integration testing with the server code. This servlet allows 
 * to bypass the SOP (Single Origin Policy) of the browser and make asynchronous calls to the 
 * server-side code. Even though the client and server code are running on the same host, SOP 
 * will prevent the client side code from making a request to the server side code due to the 
 * different ports used by client and server side code. This servlet bypasses that restriction.  
 * 
 * @author <a href="mailto:transmeta01@gmail.com">Richard Mutezintare<a/>
 *
 */
public class ProxyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private String targetServer = "http://localhost:8081/logbook";
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// Create new client to perform the proxied request
		HttpClient httpclient = new DefaultHttpClient();
		
		String reqURI = req.getRequestURI();

	/*	if(req.getRequestURI().equals("/rest/j_security_check")) {
			reqURI = "/j_security_check";
		}*/
		
		// Determine final URL 	
		StringBuffer uri = new StringBuffer();
		uri.append(targetServer);
		uri.append(reqURI);
		
//		log.info(uri.toString());
		
		// Add any supplied query strings
		String queryString = req.getQueryString();
		if (queryString != null){
			uri.append("?" + queryString);
		}
		
		// Get HTTP method
		final String method = req.getMethod();
		// Create new HTTP request container
		HttpRequestBase request = null;
		
		// Get content length
		int contentLength = req.getContentLength();

		if (contentLength < 1) {
			request = new HttpRequestBase() {
				public String getMethod() {
					return method;
				}
			};
		}
		else {
			// Prepare request
			HttpEntityEnclosingRequestBase tmpRequest = new HttpEntityEnclosingRequestBase() {
				public String getMethod() {
					return method;
				}
			};
			
			// Transfer entity body from the received request to the new request
			InputStreamEntity entity = new InputStreamEntity(req.getInputStream(), contentLength);
			tmpRequest.setEntity(entity);
			
			request = tmpRequest;
		}
		
		// Set URI
		try {
			request.setURI(new URI(uri.toString()));
		}
		catch (URISyntaxException e) {
			throw new ServletException("URISyntaxException: " + e.getMessage());
		}
		
		// Copy headers from old request to new request
		Enumeration<String> headers = req.getHeaderNames();
		while (headers.hasMoreElements()) {
			String headerName = headers.nextElement();
			String headerValue = req.getHeader(headerName);
			// Skip Content-Length and Host
			String lowerHeader = headerName.toLowerCase();
			if (lowerHeader.equals("content-length") == false && lowerHeader.equals("host") == false) {
 				System.out.println(headerName.toLowerCase() + ": " + headerValue);
				request.addHeader(headerName, headerValue);
			}
		}
		
		// Execute the request
		HttpResponse response = httpclient.execute(request);
		
		// Transfer status code to the response
		StatusLine status = response.getStatusLine();
		resp.setStatus(status.getStatusCode());
		
		System.out.println("response headers: ");
		// Transfer headers to the response
		Header[] responseHeaders = response.getAllHeaders();
		for (int i=0 ; i<responseHeaders.length ; i++) {
			Header header = responseHeaders[i];
			resp.addHeader(header.getName(), header.getValue());
			System.out.println(header.getName() + ":" + header.getValue());
		}
		
		// Transfer proxy response entity to the servlet response
		HttpEntity entity = response.getEntity();
		InputStream input = entity.getContent();
		OutputStream output = resp.getOutputStream();
		int b = input.read();
		while (b != -1) {
			output.write(b);
			b = input.read();
		}
		
		// Clean up
		input.close();
		output.close();
		httpclient.getConnectionManager().shutdown();
	}
}